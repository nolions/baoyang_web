<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public $name;
	public $tel;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('name', 'required','message'=>'姓名必須填寫'),
			array('tel', 'required','message'=>'電話必須填寫'),
			// array('email', 'required','message'=>'Email必須填寫'),
			array('subject', 'required', 'message' => '詢問事項必須填寫'),
			array('body', 'required', 'message' => '詳細情況必須填寫'),
			// email has to be a valid email address
			array('email', 'email', 'message' => ' Email格式錯誤'),
			
			array('tel', 'match', 'pattern' => '^\(?\d{2}\)?[\s\-]?\d{4}\-?\d{3}$^', 'message' => '電話號碼格式錯誤'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'name' => '姓名',
			'tel' => '電話',
			'email' => 'Email',
			'subject' => '詢問事項',
			'body' => '詳細情況',
			'verifyCode'=>'驗證碼',
		);
	}
}