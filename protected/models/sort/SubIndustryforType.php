<?php
class SubIndustryforType extends SortActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SubIndustryforType';
	}

	public function rules()
	{
		return array(
			// array('SubIndustry_id', 'required'),
			array('SubIndustry_id, Type_id', 'length', 'max'=>11),
			//array('SubIndustry_id, Type_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ProductItems' => array(self::BELONGS_TO, 'ProductItems', 'ProductItem_id'),
			
		);
	}

	public function search()
	{
		// $criteria=new CDbCriteria;

		// $criteria->compare('SubIndustry_id', $this->SubIndustry_id, true);
		// $criteria->compare('Type_id', $this->Type_id, true);

		// return new CActiveDataProvider('Industry', array(
		// 	'criteria' => $criteria,
		// 	'sort' => array(
		// 		'defaultOrder' => 'Type_id Desc',
		// 	),
		// ));
	}
}