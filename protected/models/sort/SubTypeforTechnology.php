<?php
class SubTypeforTechnology extends SortActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SubTypeforTechnology';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('SubType_id, Technology_id', 'required'),
			array('SubType_id, Technology_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Technology' => array(self::BELONGS_TO, 'Technology', 'Technology_id'),
			'SubType' => array(self::BELONGS_TO, 'SubType', 'SubType_id'),
		);
	}
}