<?php
class ProductItems extends SortActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ProductItem';
	}

	public function rules()
	{
		return array(
			//array('id, Product_name', 'required'),
			//array('name', 'length', 'max'=>50),
			array('Product_name, Product_index, Product_type, Color_1, Color_2, ContrastProduct, Exterior', 'length', 'max'=>50),

			array('Product_name, Product_index, Product_type, ContrastProduct, Exterior', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'apply'=>array(self::BELONGS_TO, 'ProductItemApply', 'id'),
			//'productapply'=>array(self::HAS_ONE, 'ProductItemApply', 'ProductItem_id '),
			//'sort'=>array(self::BELONGS_TO, 'ProductItemApply', 'id'),
		);
	}

	public function getProductItems($subIndustryType, $subProductType=0){
		$proc = "call getProduct($subIndustryType, $subProductType)";
        $command =Yii::app()->dbSort->createCommand($proc);
        $Products = $command->queryAll();

        return $Products;
	}

	public function getFilterProductItems($subIndustryType, $subProductType=0, $subTechnologyArray){
		//$proc = "call getFilterProduct($subIndustryType, $subProductType, \"23, 22\")";
		//$command =Yii::app()->dbSort->createCommand($proc);

		$SQL = "SELECT T1.*, T2.`Industry_id`, T2.`leval`, T2.`SubType_id`, T2. `SubTechnology_id`
			FROM ProductItem As T1 
			Join (SELECT A.`ProductItem_id`, A.`Industry_id`, A.`leval`, ST.`SubType_id`, ST.`SubTechnology_id`
			FROM `ProductApply` A 
			LEFT JOIN (SELECT S.`ProductItem_id`, S.`SubType_id`, I.SubTechnology_id
			FROM `ProductSort` S 
			LEFT JOIN `ProductIndex` I
			ON I.ProductItem_id = S.ProductItem_id) ST
			ON A.`ProductItem_id` = ST.`ProductItem_id`
			WHERE A.`Industry_id` IN({$subIndustryType})
			AND ST.`SubType_id` IN ({$subProductType})
			AND ST.`SubTechnology_id` IN ({$subTechnologyArray})
			GROUP BY ST.`ProductItem_id`) AS T2
			ON T1.`id` = T2.`ProductItem_id`;";

        $command =Yii::app()->dbSort->createCommand($SQL);
        $Products = $command->queryAll();

        return $Products;
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('Product_name', $this->Product_name, true);
		$criteria->compare('Product_index', $this->Product_index, true);
		$criteria->compare('Product_type', $this->Product_type, true);
		$criteria->compare('ContrastProduct', $this->ContrastProduct, true);
		$criteria->compare('Exterior', $this->Exterior, true);
		$criteria->compare('Display', $this->Display, true);
		$criteria->compare('Create_at', $this->Create_at, true);
		$criteria->compare('Update_at', $this->Update_at, true);

		return new CActiveDataProvider('ProductItems', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'Update_at Desc',
			),
		));
	}
}