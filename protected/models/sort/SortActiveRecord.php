<?php
/**
 *	This is parent class for product models.
 *	This class overwrite the get DB connections, in order to connect to different db.
 */

abstract class SortActiveRecord extends CActiveRecord
{
	public static $dbSort;
	public function getDbConnection(){
		if(self::$dbSort!==null)
			return self::$dbSort;
		else{
			self::$dbSort=Yii::app()->dbSort;
			if(self::$dbSort instanceof CDbConnection){
				self::$dbSort->setActive(true);
				return self::$dbSort;
			}else
				throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
		}
	}
}