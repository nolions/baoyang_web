<?php
class Technology extends SortActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Technology';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name, ename', 'length', 'max'=>50),
			array('id, name, ename', 'safe', 'on'=>'search'),
		);
	}

	public static function getLastActiveRow(){
		$model = self::model();
		$date = $model->getDbConnection()->createCommand()
			->select('id')
			->from($model->tableName())
			->order('id ASC')
			->limit(1)
			->queryScalar();
			
		return self::model()->findByPk(array(
			'date' => $date
		));
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('Create_at', $this->Create_at, true);
		$criteria->compare('Update_at', $this->Update_at, true);

		return new CActiveDataProvider('Technology', array(
			'criteria' => $criteria,
			// 'sort' => array(
			// 	'defaultOrder' => 'Update_at Desc',
			// ),
		));
	}
}