<?php
class SubType extends SortActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SubType';
	}

	public function rules()
	{
		return array(
			
			array('id, Type_id', 'length', 'max'=>11),
			array('name', 'length', 'max'=>50),
			array('id, name, Type_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Type' => array(self::BELONGS_TO, 'Type', 'Type_id'),
		);
	}

	public function searchbyType($TypeId = 0)
	{
		$criteria = $this->BuildCDbCriteria();
		$criteria->addCondition("Type_id=". $TypeId);

		return new CActiveDataProvider('SubType', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'id',
			),
		));
	}

	public function search()
	{
		$criteria = $this->BuildCDbCriteria();

		return new CActiveDataProvider('SubType', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'id',
			),
		));
	}

	private function BuildCDbCriteria()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('Type_id', $this->Type_id, true);
		$criteria->compare('Create_at', $this->Create_at, true);
		$criteria->compare('Update_at', $this->Update_at, true);

		return $criteria;
	}
}