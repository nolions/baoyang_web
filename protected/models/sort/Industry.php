<?php
class Industry extends SortActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Industry';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>50),
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

	public static function getLastActiveRow(){
		$model = self::model();
		$date = $model->getDbConnection()->createCommand()
			->select('id')
			->from($model->tableName())
			->order('id ASC')
			->limit(1)
			->queryScalar();
			
		return self::model()->findByPk(array(
			'date' => $date
		));
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('Create_up', $this->Create_up, true);
		$criteria->compare('Update_up', $this->Update_up, true);

		return new CActiveDataProvider('Industry', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'Update_up Desc',
			),
		));
	}
}