<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public $name;
	public $tel;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			//array('name, tel, email, subject, body', 'required'),
			array('name','required','message'=>'姓名必須填寫'),
			array('tel','required','message'=>'電話必須填寫'),
			array('email','required','message'=>'Email必須填寫'),
			array('subject','required','message'=>'電話必須填寫'),
			// email has to be a valid email address
			array('email', 'email'),
			array('tel', 'match', 'pattern' => '/^(\d{3}-|\d{4}-)(\d{8}|\d{7})?$/', 'message' => '請輸入正確電話號碼.'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
		);
	}
}