<?php

/**
 * LoginForm is the data structure for keeping user login form data.
 * It is used by the 'login' action of 'UserController'.
 */
class SetNewPasswordForm extends CFormModel {
	public $oldpassword;
	public $newpassword;
	public $repassword;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array('oldpassword, newpassword, repassword', 'required'), // userid and password are required
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'oldpassword'=>'舊密碼',
			'newpassword'=>'新密碼',
			'repassword'=>'確認密碼',
		);
	}

	public function setNewPassword(){
		$check = false;
		//098f6bcd4621d373cade4e832627b4f6
		//827ccb0eea8a706c4c34a16891f84e7b
		
		if ($this->newpassword == $this->repassword) 
		{
			$record=Users::model()->findByAttributes(array('userid'=>Yii::app()->user->userid));

			if ($record != null) {
				if($record->password == md5($this->oldpassword)){
					$record->password = md5($this->newpassword);
					$record->Update_at = date('Y-m-d H:i:s');
					$record->save();

					$check = true;
				}else
					$this->addError('oldpassword', Yii::t('site', '再次確認您的密碼是否正確'));
			}
		}else{
			$this->addError('repassword', Yii::t('site', '請再次檢查新密碼是否正確'));
		}
		
		
		return $check;
	}

}
