<?php

/**
 * BasicInfoForm is the data structure for keeping user basic information from site form data.
 */
class BasicInfoForm extends CFormModel {
	public $title;
	public $tel;
	public $fax;
	public $zip;
	public $address;
	public $email;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array('title, tel', 'required'), // userid and password are required
			array('tel', 'match', 'pattern' => '^\(?\d{2}\)?[\s\-]?\d{4}\-?\d{3}$^', 'message' => '電話號碼格式錯誤'),
			array('fax', 'match', 'pattern' => '^\(?\d{2}\)?[\s\-]?\d{4}\-?\d{3}$^', 'message' => '傳真號碼格式錯誤'),
			array('email', 'email', 'message' => ' Email格式錯誤'),
			// array('zip', 'match', 'pattern' => '^\(?! )(?!.*  )[A-Za-z0-9 ()-]*$^', 'message' => '郵遞區號格式錯誤'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'title'=>'網站名稱',
			'tel'=>'電話',
			'fax'=>'傳真',
			'zip'=>'郵遞區號',
			'address'=>'住址',
			'email'=>'Email',
		);
	}
}