<?php

/**
 * LoginForm is the data structure for keeping user login form data.
 * It is used by the 'login' action of 'UserController'.
 */
class LoginForm extends CFormModel {
	public $userid;
	public $password;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array('userid, password', 'required'), // userid and password are required
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'userid'=>'帳號',
			'password'=>'密碼',
		);
	}

	/**
	 * Logs in the user using the given email and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login() {
		$loginSuccess = false;

		
		if($this->_identity===null)
			$this->_identity=new UserIdentity($this->userid,$this->password);
		
		$pass = $this->_identity->authenticate();

		if($pass){
			$duration = 3600*24*1; // 1 days
    		Yii::app()->user->login($this->_identity, $duration);

    		$Users = Users::model()->findByAttributes(array('userid'=>$this->userid));
    		$Users->LatestLoginTime = date('Y-m-d H:i:s');
    		$Users->save();

    		$loginSuccess = true;
		}else{
    		$this->addError('userid', Yii::t('site', '帳號或密碼錯誤'));
    	}

    	return $loginSuccess;;
	}
}
