<?php
class Info extends WebActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Info';
	}

	// public function rules()
	// {
	// 	return array(
	// 		array('type', 'params', 'max'=>11),
	// 	);
	// }

	public function getSiteInfo()
	{
		$info = $this::model()->findAll(array(
		  'condition' => 'type=:type AND params=:params',
		  'limit' => 1,
		  'params' => array(':type'=>'info', ':params'=>'siteinfo'),
		));

		return $info[0]['data'];
	}

	public function getSiteAbout()
	{
		$info = $this::model()->findAll(array(
		  'condition' => 'type=:type AND params=:params',
		  'limit' => 1,
		  'params' => array(':type'=>'info', ':params'=>'aboutinfo'),
		));

		return $info[0]['data'];
	}

	public function getSiteTarget()
	{
		$info = $this::model()->findAll(array(
		  'condition' => 'type=:type AND params=:params',
		  'limit' => 1,
		  'params' => array(':type'=>'info', ':params'=>'targetinfo'),
		));

		return $info[0]['data'];
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('type', $this->type, true);
		$criteria->compare('params', $this->params, true);
		$criteria->compare('data', $this->data, true);
		$criteria->compare('Update_at', $this->Update_at, true);

		return new CActiveDataProvider('Opinion', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'Update_at Desc',
			),
		));
	}
}