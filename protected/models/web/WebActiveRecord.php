<?php
/**
 *	This is parent class for product models.
 *	This class overwrite the get DB connections, in order to connect to different db.
 */

abstract class WebActiveRecord extends CActiveRecord
{
	public static $dbWeb;
	public function getDbConnection(){
		if(self::$dbWeb!==null)
			return self::$dbWeb;
		else{
			self::$dbWeb=Yii::app()->dbWeb;
			if(self::$dbWeb instanceof CDbConnection){
				self::$dbWeb->setActive(true);
				return self::$dbWeb;
			}else
				throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
		}
	}
}