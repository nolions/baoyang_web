<?php
class Opinion extends AdminActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'opinion';
	}

	public function rules()
	{
		return array(
			array('id', 'length', 'max'=>10),
			array('name, tel, email, title', 'length', 'max'=>50),
			array('id, name, tel, email, title, content, Create_at', 'safe', 'on'=>'search'),
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('tel', $this->tel, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('Create_at', $this->Create_at, true);

		return new CActiveDataProvider('Opinion', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'Create_at Desc',
			),
		));
	}
}