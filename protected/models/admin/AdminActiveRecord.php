<?php
/**
 *	This is parent class for product models.
 *	This class overwrite the get DB connections, in order to connect to different db.
 */

abstract class AdminActiveRecord extends CActiveRecord
{
	public static $dbAdmin;
	public function getDbConnection(){
		if(self::$dbAdmin!==null)
			return self::$dbAdmin;
		else{
			self::$dbAdmin=Yii::app()->dbAdmin;
			if(self::$dbAdmin instanceof CDbConnection){
				self::$dbAdmin->setActive(true);
				return self::$dbAdmin;
			}else
				throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
		}
	}
}