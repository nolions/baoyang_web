﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// 
	//config.allowedContent = true;
	// config.extraPlugins = "lineutils,widget,leaflet";

	config.filebrowserBrowseUrl = 'ckfinder/ckfinder.html'; 
	config.filebrowserImageBrowseUrl = 'ckfinder/ckfinder.html?Type=Images'; 
	config.filebrowserFlashBrowseUrl = 'ckfinder/ckfinder.html?Type=Flash'; 
	config.filebrowserUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'; //可上傳一般檔案 
	config.filebrowserImageUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';//可上傳圖檔 
	config.filebrowserFlashUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';//可上傳Flash檔案
	config.enterMode = CKEDITOR.ENTER_BR;
	config.shiftEnterMode = CKEDITOR.ENTER_BR;

	config.toolbar = 'Custom'; //自訂toolbar類型
	config.toolbar_Custom = //詳細設定(需加"_名稱")
	[
		{ name: 'styles', items : ['FontSize','Font','Styles','Format'] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat'] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		'/',
		{ name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor'] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','Scayt' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','Subscript', 'Superscript'  ] },
	];

};
