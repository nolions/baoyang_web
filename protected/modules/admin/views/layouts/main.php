<?php
	$assetUrl = publish('application.assets');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Baoyang - Admin</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<?php
//
	$industryArray = array();
	$siteInfoArray = array();
	$suggestArray = array();
	$userArray = array();
	$backupArray = array();

	if(!Yii::app()->user->isGuest){
		$industryArray  = array(
			array('label'=>'產業類別', 'url'=>$this->createUrl('industry/')),
			array('label'=>'產品類型', 'url'=>$this->createUrl('type/')),
			array('label'=>'數據指標', 'url'=>$this->createUrl('technology/')),
			array('label'=>'產品資料', 'url'=>$this->createUrl('itemlist/'))
		);

		$siteInfoArray = array(
			array('label'=>'博洋資訊', 'url'=>$this->createUrl('info/basicinfo')),
			array('label'=>'博洋簡介', 'url'=>$this->createUrl('info/editabout')),
			array('label'=>'博洋願景', 'url'=>$this->createUrl('info/edittarget')),			
		);

		$suggestArray = array(
			array('label'=>'客戶意見', 'url'=>$this->createUrl('suggest/index'))
		);

		$backupArray = array(
			array('label'=>'備份產品資訊', 'url'=>$this->createUrl('backup/sortdb')),
			array('label'=>'備份網站資訊', 'url'=>$this->createUrl('backup/webdb')),
			array('label'=>'備份其他資訊', 'url'=>$this->createUrl('backup/admindb'))
		);

		$userArray = array(
			array('label'=>'變更密碼', 'url'=>$this->createUrl('user/setnewpassword')),
			array('label'=>'登出', 'url'=>$this->createUrl('user/logout'))
		);
	}
?>
<body>
	<?php $this->widget('bootstrap.widgets.BootNavbar', array(
	'fixed'=>true,
	'brand'=>'Baoyang - Admin',
	'brandUrl'=>'default/',
	//'collapse'=>true, // requires bootstrap-responsive.css
	'items'=>array(
		array(
			'class'=>'bootstrap.widgets.BootMenu',
			'items'=>array(
				array('label'=>'產品資料', 'items' => $industryArray),
				array('label'=>'網站資訊', 'items' => $siteInfoArray),
				array('label'=>'客戶意見', 'items' => $suggestArray),
				array('label'=>'備份資料', 'items' => $backupArray),
				array('label'=>'帳戶管理', 'items' => $userArray),
			),
		),
	),
)); ?>

	<div class="content container">
		<div class="breadcrumb-panel">
			<?php if ( !empty($this->breadcrumbs)): ?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'homeLink' => false,
					'links'=>$this->breadcrumbs,
					'separator' => '<span class="divider">/</span>',
					'htmlOptions' => array(
						'class' => 'breadcrumb',
					),
				));?>
			<?php endif ?>
		</div>

		<?php echo $content; ?>
	</div>
	<footer style="margin-top:50px;">
		<div class="container">
			<p>&copy; Copyright <?=date('Y')?> Baoyang Lab. All Rights Reserved.</p>
		</div>
	</footer>
</body>
</html>