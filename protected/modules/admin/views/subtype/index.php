<style type="text/css">
	#industry-grid input{
		width: 95%;
	}

	.filters  td{
		text-align: center;
	}
</style>
<?php
$this->breadcrumbs=array(
	'產品子類型管理',
);

$this->widget('application.widgets.BlueTab.BlueTab', array(
	'items' => array(
				'產品類型'=>$this->createUrl('type/'),
				'子類型'=>$this->createUrl('subtype/'),
				),
	'on' => 1,
));
?>

<div>
	<div style="width:120px;">
		<?php echo CHtml::link('新增子類型',array('subtype/create'),
			array('id'=>'filterBtn', 'class' => 'btn_blue_send')); ?>
	</div>

	<div style="margin-top:20px;">
		產品類型：
	<?php 
		$TypOpts = CHtml::listData($types, 'id', 'name');

		echo CHtml::dropDownList('TypeSel', 'TypeVal', $TypOpts, 
			array(
				'class'=>'span2',
				'style' => (count($TypOpts)<=0)?'display: none;':'display:inline-block;',
				'ajax' => array(
					'type' => 'GET',
					'url'=>CController::createUrl('subtype/AjaxGetSubType'),
					'update' => '#DataList',
					'data' => array(
						"Type"=>'js:$("#TypeSel").val()',
					),
				)
			)
		); 
	?>
	</div>

	<div id="DataList" class="SubIndustryContent" style="">
	<?php $this->renderPartial('_Table', array(
			'data' => $data,
		)); ?>
	</div>
</div>
<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScript('autoLoad', "
	$('#filterBtn').click(function(event){
		var Type = $('#TypeSel').val();
		window.location.href='subtype/create/Type/'+Type; 
		return false;
	});
");
// $cs->registerScriptFile($moduleAssetUrl.'/js/productmanage/main.js', CClientScript::POS_END);
?>