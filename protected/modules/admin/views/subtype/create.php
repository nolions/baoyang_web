<?php
$this->breadcrumbs=array(
	'產品子類型管理' => array('subtype/'),
	'新增產品子類型',
);
?>

<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'stockalias-form',
	'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($model); ?>
	<div class="">
		<?php echo $form->labelEx($model,'產品子類型名稱：',array('style' => 'display:initial;'));?>
		<?php echo $form->textField($model,'name');?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_down"><span>隸屬產品類型</span></div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="">
				<tr>
					<td width="100">產業類別</td>
					<td>
					<?php echo $form->radioButtonList(
						$model,
						'Type_id', 
						CHtml::listData($Types, 'id', 'name'),

						array(
							'labelOptions'=>array('style'=>'display:inline; margin-right: 10px;'), // add this code
							'separator'=>'',
							'class' => '1',
						)
					); ?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="buttons" style="">
		<?php echo CHtml::submitButton($model->isNewRecord ? '新增' : '更新', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
		<?php echo CHtml::resetButton('Reset', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
	</div>

<?php $this->endWidget(); ?>
</div>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScriptFile($moduleAssetUrl.'/js/main.js', CClientScript::POS_END);
?>