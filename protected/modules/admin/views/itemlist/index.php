<?php
$assetUrl = publish('application.assets');

$this->breadcrumbs=array(
	'產品資料管理',
);
?>
<style type="text/css">
	#products-grid td{
		text-align: center;
	}
	#products-grid input{
		width: 94%;
	}
</style>
<div>
	<div style="width:100px;">
		<?=CHtml::link('新增產品', Yii::app()->createUrl("admin/itemlist/create"),
				array('id'=>'filterBtn', 'class' => 'btn_blue_send')
		);?>
	</div>

	<div class="IndustryContent" style="">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'products-grid',
	    'dataProvider'=> $data,
	    'filter' => $model,
	    // 'afterAjaxUpdate'=>'function(id, data){
     //    	$("[id^=state_id_]").wrap("<label class=list></label>");
     //    	$("<span></span>").insertAfter("[id^=state_id_]");      
    	// }',
	    'columns'=>array(
	    	// array(         
	     //        // 'name' => 'id',
	     //        'header'=>'顯示',
	     //        // 'class'=>'CCheckBoxColumn',
	     //        // 'selectableRows' => '20',
	     //        'value'=>'$data->id',
	     //        // 'visible'=>true,
	     //        'value'=>'Yii::app()->controller->isChecked($data->Display)',
	     //        // 'disabled'=>'1',
	     //        // 'checked'=>'Yii::app()->controller->isChecked($data->id)',
	     //        'htmlOptions' => array('style'=>'text-align: center;'),
      //   	),
	    	array(            // display 'create_time' using an expression
	            'header'=>'產品品名',
	            'name'=>'Product_name',
	            'htmlOptions' => array('style'=>'text-align: center; width:100px;'),
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'國際索引碼',
	            'name'=>'Product_index',
	            'htmlOptions' => array('style'=>'text-align: center; width:100px;'),
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'對照產品',
	            'name'=>'ContrastProduct',
	            'htmlOptions' => array('style'=>'text-align: left;'),
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'類型',
	            'name'=>'Product_type',
	            'htmlOptions' => array('style'=>'text-align: center; width:80px;'),
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'外觀顏色',
	            'name'=>'Exterior',
	            'htmlOptions' => array('style'=>'text-align: center; width:80px;'),
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'更新時間',
	            'name'=>'Update_at',
	            'filter'=>false,
	            'htmlOptions' => array('style'=>'text-align: center; width:80px;'),
	        ),
	        array(
	        	'class'=>'CButtonColumn',
	        	'template'=>'{view}{update}{delete}',
	        	'buttons'=>array(
	        		'view' => array(
	        			'url' => 'Yii::app()->createUrl("admin/itemlist/view", array("id"=>$data->id))',
	        			'options' => array('class'=>'test')
	        		),
	        		'update' => array(
	        			'url' => 'Yii::app()->createUrl("admin/itemlist/update", array("id"=>$data->id))',
	        			'options' => array('class'=>'test')
	        		),
	        		'delete' => array(
	        			'url' => 'Yii::app()->createUrl("admin/itemlist/delete", array("id"=>$data->id))',
	        			// 'options' => array('class'=>'test'),
	        		),
    			)
	        ),
	    ),
	)); ?>
	</div>

</div>
<?php
$assetUrl = publish('application.assets');
Yii::app()->getClientScript()->registerCssFile($assetUrl.'/css/button.css');
?>
