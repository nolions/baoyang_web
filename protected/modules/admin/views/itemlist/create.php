<?php
$this->breadcrumbs=array(
	'產品資料管理' => array('itemlist/'),
	'新增產品',
);

?>

<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'stockalias-form',
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($model); ?>
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header"><th colspan="3">基本資訊</th></tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody>
				<tr><td></td><td></td></tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'產品品名：',array('style' => 'display:initial;'));?>
					</td>
					<td>
						<?php echo $form->textField($model,'Product_name');?>
					</td>
					<td>
						<?php echo $form->error($model,'Product_name'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'國際索引碼：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php echo $form->textField($model,'Product_index'); ?>
					</td>
					<td>
						<?php echo $form->error($model,'Product_index'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'對照產品：', array('style' => 'display:initial;')); ?>
					</td>
					<td><?php echo $form->textField($model,'ContrastProduct'); ?></td>
					<td><?php echo $form->error($model,'ContrastProduct'); ?></td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'類型：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php echo $form->textField($model,'Product_type'); ?>
					</td>
					<td>
						<?php echo $form->error($model,'Product_type'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'外觀顏色：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php echo $form->textField($model,'Exterior'); ?>
					</td>
					<td>
						<?php echo $form->error($model,'Exterior'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'本色(色卡)：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php $form->textField($model,'Color_1'); ?>
						<?php $this->widget('application.extensions.SMiniColors.SActiveColorPicker', array(
								'model' => $model,
    							'attribute' => 'Color_1',
    							'hidden'=>false, // defaults to false - can be set to hide the textarea with the hex
    							'options' => array(), // jQuery plugin options
    							'htmlOptions' => array(), // html attributes
    					)); ?>
					</td>
					<td>
						<?php echo $form->error($model,'Color_1'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'沖淡色：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php $this->widget('application.extensions.SMiniColors.SActiveColorPicker', array(
								'model' => $model,
    							'attribute' => 'Color_2',
    							'hidden'=>false, // defaults to false - can be set to hide the textarea with the hex
    							'options' => array(), // jQuery plugin options
    							'htmlOptions' => array(), // html attributes
    					)); ?>
						<?php //echo $form->textField($model,'Color_2'); ?>
					</td>
					<td>
						<?php echo $form->error($model,'Color_2'); ?>
					</td>
				</tr>
				<!-- <tr>
					<td width="100">
						<?php //echo $form->labelEx($model,'物性數據：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php //echo $form->textField($model,'Target'); ?>
					</td>
					<td>
					</td>
				</tr> -->
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'技術指標：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php //echo $form->textField($model,'Technology'); ?>
						<?php echo $form->textArea($model,'Technology', array(
							'style' => 'width:400px;', 'maxlength' => 300, 'rows' => 6,)
						); ?>
					</td>
					<td>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'物性數據：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php //echo $form->textField($model,'Physical'); ?>
						<?php echo $form->textArea($model,'Physical', array(
							'style' => 'width:400px;', 'maxlength' => 300, 'rows' => 6,)
						); ?>
					</td>
					<td>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'成份資料：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php //echo $form->textField($model,'Material'); ?>
						<?php echo $form->textArea($model,'Material', array(
							'style' => 'width:400px;', 'maxlength' => 300, 'rows' => 6,)
						); ?>
					</td>
					<td>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'其他：', array('style' => 'display:initial;')); ?>
					</td>
					<td>
						<?php //echo $form->textField($model,'Other'); ?>
						<?php echo $form->textArea($model,'Other', array(
							'style' => 'width:400px;', 'maxlength' => 300, 'rows' => 6,)
						); ?>
					</td>
					<td>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<hr>
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_right">產業類型</div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="display:none;">
				<tr>
					<td colspan="2" style="padding-bottom:10px;">
						<div>
							<a href="#" id="IndustrySelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">全選</a>
							<a href="#" id="IndustryunSelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">取消全選</a>
							<a href="#" id="IndustryuSel_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">反向選取</a>
							<div class="clear"></div>
						</div>
					</td>
				</tr>
			<?php foreach ($Industrys as $key => $value) :
				$SubIndustrys_list=CHtml::listData(SubIndustry::model()->findAll('Industry_id=:Industry_id', array(
					':Industry_id'=>$value['id'])),'id','name'); ?>
				<tr>
					<td width="100" style="text-align:right;"><?php echo $value['name']."：";?></td>
					<td>
					<?php echo CHtml::checkBoxList('Industrys','',$SubIndustrys_list, array(
						'labelOptions' => array(
							'style' => "display: inline-block; margin-right: 10px;",
						),
			           	'separator'=>'',
			           	'class' => 'IndustryItem',
			           	'baseID' => 'Industry_'.$value['id'],
					)); ?>
					</td>
				</tr>	
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<hr>
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_right">產品類型</div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="display:none;">
				<tr>
					<td colspan="2" style="padding-bottom:10px;">
						<div>
							<a href="#" id="TypeSelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">全選</a>
							<a href="#" id="TypeunSelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">取消全選</a>
							<a href="#" id="TypeuSel_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">反向選取</a>
							<div class="clear"></div>
						</div>
					</td>
				</tr>
			<?php foreach ($Types as $key => $value) :
				$SubTypes_list=CHtml::listData(SubType::model()->findAll('Type_id=:Type_id', array(
					':Type_id'=>$value['id'])),'id','name'); ?>
				<tr>
					<td width="150" style="text-align:right;"><?php echo $value['name']."：";?></td>
					<td>
					<?php echo CHtml::checkBoxList('Types','',$SubTypes_list, array(
						'labelOptions' => array(
							'style' => "display: inline-block; margin-right: 10px;",
						),
			           	'separator'=>'',
			           	'class' => 'TypeItem',
			           	'baseID' => 'Type_'.$value['id'],
					)); ?>
					</td>
				</tr>	
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<hr>
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_right">數據指標</div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="display:none;">
				<tr>
					<td colspan="2" style="padding-bottom:10px;">
						<div>
							<a href="#" id="TechnologySelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">全選</a>
							<a href="#" id="TechnologyunSelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">取消全選</a>
							<a href="#" id="TechnologyuSel_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">反向選取</a>
							<div class="clear"></div>
						</div>
					</td>
				</tr>
			<?php foreach ($Technologys as $key => $value) :
				$SubTechnologys_list=CHtml::listData(SubTechnology::model()->findAll('Technology_id=:Technology_id', array(
					':Technology_id'=>$value['id'])),'id','name'); ?>
				<tr>
					<td width="150" style="text-align:right;"><?php echo $value['name']."：";?></td>
					<td>
					<?php echo CHtml::checkBoxList('Technology','',$SubTechnologys_list, array(
						'labelOptions' => array(
							'style' => "display: inline-block; margin-right: 10px;",
						),
			           	'separator'=>'',
			           	'class' => 'TechnologyItem',
			           	'baseID' => 'Technology_'.$value['id'],
					)); ?>
					</td>
				</tr>	
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<div class="buttons" style="">
		<?php echo CHtml::submitButton($model->isNewRecord ? '新增' : '更新', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
		<?php echo CHtml::resetButton('Reset', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/main.css');
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScriptFile($moduleAssetUrl.'/js/main.js', CClientScript::POS_END);
$cs->registerScriptFile($moduleAssetUrl.'/js/itemlist/index.js', CClientScript::POS_END);
$cs->registerScriptFile($assetUrl.'/js/checkboxsel.js', CClientScript::POS_END);
?>