<?php
$this->breadcrumbs=array(
	'產品資料管理' => array('itemlist/'),
	$model->id .": " . $model->Product_name ,
);
?>
<div>
	<div class="">
		<div class="bascinfo_header header">基本資訊</div>
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				array(
					'label'=>'產品名稱：',
					'type'=>'raw',
					'value'=>$model->Product_name,
					// 'visible'=> false,
				),
				array(
					'label'=>'國際索引碼：',
					'type'=>'raw',
					'value'=>$model->Product_index,
					'visible'=> ($model->Product_index == null || $model->Product_index =='')?false:true,
				),
				array(
					'label'=>'對照產品：',
					'type'=>'raw',
					'value'=>$model->ContrastProduct,
					'visible'=> ($model->ContrastProduct == null || $model->ContrastProduct =='')?false:true,
				),
				array(
					'label'=>'類型',
					'type'=>'raw',
					'value'=>$model->Product_type,
					'visible'=> ($model->Product_type == null || $model->Product_type =='')?false:true,
				),
				array(
					'label'=>'外觀：',
					'type'=>'raw',
					'value'=>$model->Exterior,
					'visible'=> ($model->Exterior == null || $model->Exterior =='')?false:true,
				),
				array(
					'label'=>'本色(色卡)：',
					'type'=>'raw',
					'value'=>$model->Color_1,
					'template'=>"<tr class='{class}'><th>{label}</th><td><div style='background:{value};'>{value}</div></td></tr>",
					'visible'=> ($model->Color_1 == null || $model->Color_1 =='')?false:true,
				),
				array(
					'label' => '沖淡色：',
					'type' => 'raw',
					'value' => $model->Color_2,
					'template'=>"<tr class='{class}'><th>{label}</th><td><div style='background:{value};'>{value}</div></td></tr>",
					'visible' => ($model->Color_2 == null || $model->Color_2 =='')?false:true,
				),
				array(
					'label'=>'數據指標：',
					'type'=>'raw',
					'value'=>$model->Target,
					'visible'=> ($model->Target == null || $model->Target =='')?false:true,
				),
				array(
					'label'=>'技術指標：',
					'type'=>'raw',
					'value'=>$model->Technology,
					'visible'=> ($model->Technology == null || $model->Technology =='')?false:true,
				),
				array(
					'label'=>'數性數據：',
					'type'=>'raw',
					'value'=>$model->Physical,
					'visible'=> ($model->Physical == null || $model->Physical =='')?false:true,
				),
				array(
					'label'=>'成份資料：',
					'type'=>'raw',
					'value'=>$model->Material,
					'visible'=> ($model->Material == null || $model->Material =='')?false:true,
				),
				array(
					'label'=>'其他：',
					'type'=>'raw',
					'value'=>$model->Other,
					'visible'=> ($model->Other == null || $model->Other =='')?false:true,
				),
				array(
					'label'=>'更新時間：',
					'type'=>'raw',
					'value'=>$model->Update_at,
					'visible'=> ($model->Update_at == null || $model->Update_at =='')?false:true,
				),
			),
		)); ?>
	</div>
	<hr>
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_down">產業類型</div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="">
			<?php foreach ($Industrys as $key => $value) :
				$SubIndustrys_list=CHtml::listData(SubIndustry::model()->findAll('Industry_id=:Industry_id', array(
					':Industry_id'=>$value['id'])),'id','name'); ?>
				<tr>
					<td width="100"><?php echo $value['name']."：";?></td>
					<td>
					<?php echo CHtml::checkBoxList('Industrys', $IndustrysApplys, $SubIndustrys_list, array(
						'labelOptions' => array(
							'style' => "display: inline-block; margin-right: 10px;",
						),
						'disabled'=>true,
			           	'separator'=>'',
					)); ?>
					</td>
				</tr>	
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<hr>
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_down">產品類型</div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="">
			<?php foreach ($Types as $key => $value) :
				$SubTypes_list=CHtml::listData(SubType::model()->findAll('Type_id=:Type_id', array(
					':Type_id'=>$value['id'])),'id','name'); ?>
				<tr>
					<td width="150"><?php echo $value['name']."：";?></td>
					<td>
					<?php echo CHtml::checkBoxList('Types', $ProductTypes, $SubTypes_list, array(
						'labelOptions' => array(
							'style' => "display: inline-block; margin-right: 10px;",
						),
						'disabled'=>true,
			           	'separator'=>'',
					)); ?>
					</td>
				</tr>	
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<hr>
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_down">數據指標</div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="">
			<?php foreach ($Technologys as $key => $value) :
				$SubTechnologys_list=CHtml::listData(SubTechnology::model()->findAll('Technology_id=:Technology_id', array(
					':Technology_id'=>$value['id'])),'id','name'); ?>
				<tr>
					<td width="150"><?php echo $value['name']."：";?></td>
					<td>
					<?php echo CHtml::checkBoxList('Technology', $TechnologyIndexs, $SubTechnologys_list, array(
						'labelOptions' => array(
							'style' => "display: inline-block; margin-right: 10px;",
						),
						'disabled'=>true,
			           	'separator'=>'',
					)); ?>
					</td>
				</tr>	
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScriptFile($moduleAssetUrl.'/js/main.js', CClientScript::POS_END);
?>