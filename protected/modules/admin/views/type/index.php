<style type="text/css">
	
	#industry-grid input{
		width: 95%;
	}

	.filters  td{
		text-align: center;
	}
</style>
<?php
$this->breadcrumbs=array(
	'產品類型管理',
);

$this->widget('application.widgets.BlueTab.BlueTab', array(
	'items' => array(
				'產品類型'=>$this->createUrl('type/'),
				'子類型'=>$this->createUrl('subtype/'),
				),
	'on' => 0,
));
?>
<div>
	<div style="width:120px;">
		<?=CHtml::link('新增產品類型', Yii::app()->createUrl("admin/type/create"), 		
			array('id'=>'filterBtn', 'class' => 'btn_blue_send')
		);?>
	</div>

	<div class="IndustryContent" style="">
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'industry-grid',
	    'dataProvider'=> $data,
	    'filter' => $model,
	    'columns'=>array(
	    	// array(            // display 'create_time' using an expression
	    		// 'header' => '編號',
	    		// 'name' => 'id',
	    		// 'htmlOptions' => array('width'=> '50px'),
	        // ),
	        array(            // display 'create_time' using an expression
	            'header'=>'產品類型',
	            'name'=>'name',
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'更新時間',
	            'name'=>'Update_at',
	            'htmlOptions' => array('width'=> '150px', 'style'=>'text-align:center;'),
	        ),
	        array(
	        	'class'=>'CButtonColumn',
	        	'template'=>'{view}{update}{delete}',
	        	'buttons'=>array(
	        		'view' => array(
	        			'url' => 'Yii::app()->createUrl("admin/type/view", array("id"=>$data->id))',
	        			'options' => array('class'=>'test')
	        		),
	        		'update' => array(
	        			'url' => 'Yii::app()->createUrl("admin/type/update", array("id"=>$data->id))',
	        			'options' => array('class'=>'test')
	        		),
	        		'delete' => array(
	        			'url' => 'Yii::app()->createUrl("admin/type/delete", array("id"=>$data->id))',
	        			// 'options' => array('class'=>'test'),
	        		),
    			)
	        ),
	    ),
	));
	?>
	</div>
</div>
<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
// $cs->registerScriptFile($moduleAssetUrl.'/js/productmanage/main.js', CClientScript::POS_END);
?>