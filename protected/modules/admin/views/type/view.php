<?php
$this->breadcrumbs=array(
	'產品類型管理' => array('type/'),
	$model->id .": " . $model->name ,
);
?>
<div>
	<div class="">
		<div class="bascinfo_header header">基本資訊</div>
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				array(
					'label'=>'產品類型：',
					'type'=>'raw',
					'value'=>$model->name,
					// 'visible'=> false,
				),
				array(
					'label'=>'更新時間：',
					'type'=>'raw',
					'value'=>$model->Update_at,
					'visible'=> ($model->Update_at == null || $model->Update_at =='')?false:true,
				),
			),
		)); ?>
	</div>
	<hr>
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_down">應用產業</div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="">
			<?php foreach ($Industrys as $key => $value) :
				$SubIndustrys_list=CHtml::listData(SubIndustry::model()->findAll('Industry_id=:Industry_id', array(
					':Industry_id'=>$value['id'])),'id','name'); ?>
				<tr>
					<td width="100"><?php echo $value['name']."：";?></td>
					<td>
					<?php echo CHtml::checkBoxList('Industrys', $IndustrysApplys, $SubIndustrys_list, array(
						'labelOptions' => array(
							'style' => "display: inline-block; margin-right: 10px;",
						),
						'disabled'=>true,
			           	'separator'=>'',
					)); ?>
					</td>
				</tr>	
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScriptFile($moduleAssetUrl.'/js/main.js', CClientScript::POS_END);
?>