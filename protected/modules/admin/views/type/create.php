<?php
$this->breadcrumbs=array(
	'產品類型管理' => array('type/'),
	'新增產品類型',
);
?>

<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'stockalias-form',
	'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($model); ?>
	<div class="">
		<?php echo $form->labelEx($model,'類型名稱：',array('style' => 'display:initial;'));?>
		<?php echo $form->textField($model,'name');?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<hr>
	
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_down">應用產業</div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="">
				<tr>
					<td colspan="2" style="padding-bottom:10px;">
						<div>
							<a href="#" id="SelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">全選</a>
							<a href="#" id="unSelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">取消全選</a>
							<a href="#" id="uSel_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">反向選取</a>
							<div class="clear"></div>
						</div>
					</td>
				</tr>
			<?php foreach ($Industrys as $key => $value) :
			// print_r($value);
				$SubIndustrys_list=CHtml::listData(SubIndustry::model()->findAll('Industry_id=:Industry_id', array(
					':Industry_id'=>$value['id'])),'id','name'); ?>
				<tr>
					<td width="100"><?php echo $value['name']."：";?></td>
					<td>
					<?php echo CHtml::checkBoxList('Industrys','',$SubIndustrys_list, array(
						'labelOptions' => array(
							'style' => "display: inline-block; margin-right: 10px;",
						),
			           	'separator'=>'',
			           	'class' => 'TypeItem',
			           	'baseID' => $value['id'],
					)); ?>
					</td>
				</tr>	
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<div class="buttons" style="">
		<?php echo CHtml::submitButton($model->isNewRecord ? '新增' : '更新', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
		<?php echo CHtml::resetButton('Reset', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
	</div>

<?php $this->endWidget(); ?>
</div>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/main.css');
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScriptFile($assetUrl.'/js/checkboxsel.js', CClientScript::POS_END);
$cs->registerScriptFile($moduleAssetUrl.'/js/main.js', CClientScript::POS_END);
$cs->registerScriptFile($moduleAssetUrl.'/js/type/index.js', CClientScript::POS_END);
?>