<?php
$this->breadcrumbs=array(
	'數據指標管理' => array('technology/'),
	'新增數據指標',
);
?>

<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'stockalias-form',
	'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($model); ?>
	<div class="">
		<?php echo $form->labelEx($model,'指標名稱：',array('style' => 'display:initial;'));?>
		<?php echo $form->textField($model,'name');?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<hr>
	
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header">
					<th colspan="2"><a href="#"><div class="arrow arrow_down">應用產品</div></a></th>
				</tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody style="">
				<tr>
					<td colspan="2" style="padding-bottom:10px;">
						<div>
							<a href="#" id="SelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">全選</a>
							<a href="#" id="unSelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">取消全選</a>
							<a href="#" id="uSel_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">反向選取</a>
							<div class="clear"></div>
						</div>
					</td>
				</tr>
			<?php foreach ($Types as $key => $value) :
				$SubTypes_list=CHtml::listData(SubType::model()->findAll('Type_id=:Type_id', array(
					':Type_id'=>$value['id'])),'id','name'); ?>
				<tr>
					<td width="140"><?php echo $value['name']."：";?></td>
					<td>
					<?php echo CHtml::checkBoxList('SubType','',$SubTypes_list, array(
						'labelOptions' => array(
							'style' => "display: inline-block; margin-right: 10px;",
						),
			           	'separator'=>'',
			           	'class' => 'TechnologyItem',
			           	'baseID' => $value['id'],
					)); ?>
					</td>
				</tr>	
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<div class="buttons" style="">
		<?php echo CHtml::submitButton($model->isNewRecord ? '新增' : '更新', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
		<?php echo CHtml::resetButton('Reset', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
	</div>

<?php $this->endWidget(); ?>
</div>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/main.css');
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScriptFile($assetUrl.'/js/checkboxsel.js', CClientScript::POS_END);
$cs->registerScriptFile($moduleAssetUrl.'/js/main.js', CClientScript::POS_END);
$cs->registerScriptFile($moduleAssetUrl.'/js/technology/index.js', CClientScript::POS_END);
?>