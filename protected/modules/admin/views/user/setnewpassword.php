<?php
$this->breadcrumbs=array(
	'變更密碼',
);

	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	));
?>
<center>
<div style="width:400px;">
	
	<table class="UserTable">
		<thead>
			<tr class="header" style="line-height:40px;">
				<th colspan="2">Baoyan Admin - 變更密碼</th>
			</tr>
			<tr><td></td><td></td></tr>
		</thead>
		<tbody>
			<tr style="height:10px;"></tr>
			<tr>
				<td width="50"></td>
				<td width="300" class="explanation"><?php echo $form->error($model,'oldpassword'); ?></td>
			</tr>
			<tr>
				<td width="70"><?php echo $form->labelEx($model,'oldpassword'); ?></td>
				<td width="300"><?php echo $form->passwordField($model,'oldpassword', array('style'=>'width:300px;')); ?></td>
			</tr>
			<tr>
				<td width="50"></td>
				<td width="300" class="explanation"><?php echo $form->error($model,'newpassword'); ?></td>
			</tr>
			<tr>
				<td width="70"><?php echo $form->labelEx($model,'newpassword'); ?></td>
				<td width="300"><?php echo $form->passwordField($model,'newpassword', array('style'=>'width:300px;')); ?></td>
			</tr>

			<tr>
				<td width="50"></td>
				<td width="300" class="explanation"><?php echo $form->error($model,'repassword'); ?></td>
			</tr>
			<tr>
				<td width="70"><?php echo $form->labelEx($model,'repassword'); ?></td>
				<td width="300"><?php echo $form->passwordField($model,'repassword', array('style'=>'width:300px;')); ?></td>
			</tr>
			
			<tr><td></td><td></td></tr>
			<tr>
				<td colspan="2">
					<?php echo CHtml::submitButton('確定',  array(
						'class'=>'btn_red_send', 'style'=>'width:95%;display:initial;'
					)); ?>
				</td>
			</tr>
			<tr style="height:10px;"></tr>
		</tbody>
	</table>


</div>
</center>
  

<?php $this->endWidget(); ?>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/main.css');
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/user/main.css');
?>