<?php 
	$this->layout = 'login';
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	));
?>
<center>
<div style="width:400px;position:relative;top:100px;">
	
	<table class="UserTable">
		<thead>
			<tr class="header" style="line-height:40px;">
				<th colspan="2">登入Baoyan Admin</th>
			</tr>
			<tr><td></td><td></td></tr>
		</thead>
		<tbody>
			<tr style="height:10px;"></tr>
			<tr>
				<td width="50"></td>
				<td width="300" class="explanation"><?php echo $form->error($model,'userid'); ?></td>
			</tr>
			<tr>
				<td width="70"><?php echo $form->labelEx($model,'userid'); ?></td>
				<td width="300"><?php echo $form->textField($model,'userid', array('style'=>'width:300px;')); ?></td>
			</tr>
			
			<tr>
				<td width="50"></td>
				<td width="300" class="explanation"><?php echo $form->error($model,'password'); ?></td>
			</tr>
			<tr>
				<td width="70"><?php echo $form->labelEx($model,'password'); ?></td>
				<td width="300"><?php echo $form->passwordField($model,'password', array('style'=>'width:300px;')); ?></td>
			</tr>
			
			<tr><td></td><td></td></tr>
			<tr>
				<td colspan="2">
					<?php echo CHtml::submitButton('Login',  array(
						'class'=>'btn_red_send', 'style'=>'width:95%;display:initial;'
					)); ?>
				</td>
			</tr>
			<tr style="height:10px;"></tr>
		</tbody>
	</table>


</div>
</center>
  

<?php $this->endWidget(); ?>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/main.css');
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/user/main.css');
?>