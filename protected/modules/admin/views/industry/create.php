<?php
$this->breadcrumbs=array(
	'產業類別管理' => array('industry/'),
	'新增產業類別',
);
?>

<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'stockalias-form',
	'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($model); ?>
	<div class="">
		<?php echo $form->labelEx($model,'產業名稱：',array('style' => 'display:initial;'));?>
		<?php echo $form->textField($model,'name');?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="buttons" style="">
		<?php echo CHtml::submitButton($model->isNewRecord ? '新增' : '更新', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
		<?php echo CHtml::resetButton('Reset', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
	</div>

<?php $this->endWidget(); ?>
</div>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
?>