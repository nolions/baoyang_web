<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

if(Yii::app()->user->isGuest):
?>
<center>
	
	<div style="width:300px;">
		<?=CHtml::link(CHtml::image($assetUrl .'/images/lock.png').'請先進行登入', Yii::app()->createUrl("admin/user/login"), 		
			array('id'=>'filterBtn', 'class' => 'btn_red_send', 'style' =>'height:100px;line-height:100px;font-size:30px;')
		);?>
	</div>
</center>
<?php endif ?>

<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
// $cs->registerScriptFile($moduleAssetUrl.'/js/productmanage/main.js', CClientScript::POS_END);
?>