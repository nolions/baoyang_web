<?php
$this->breadcrumbs=array(
	'數據指標子項目管理' => array('subtechnology/'),
	$model->id .": " . $model->name ,
);
?>

<div class="">
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			array(
				'label'=>'子項目名稱：',
				'type'=>'raw',
				'value'=>$model->name,
				// 'visible'=> false,
			),
			array(
				'label'=>'隸屬數據指標：',
				'type'=>'raw',
				'value'=>$model->Technology->name,
				// 'visible'=> false,
			),
			array(
				'label'=>'更新時間：',
				'type'=>'raw',
				'value'=>$model->Update_at,
				'visible'=> ($model->Update_at == null || $model->Update_at =='')?false:true,
			),
		),
	)); ?>
</div>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
?>