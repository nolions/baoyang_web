	<style type="text/css">
	#industry-grid input{
		width: 95%;
	}

	.filters  td{
		text-align: center;
	}
</style>
<?php
$this->breadcrumbs=array(
	'數據指標子項目管理',
);

$this->widget('application.widgets.BlueTab.BlueTab', array(
	'items' => array(
				'數據指標類型'=>$this->createUrl('technology/'),
				'子項目'=>$this->createUrl('subtechnology/'),
				),
	'on' => 1,
));
?>

<div>
	<div style="width:120px;">
		<?=CHtml::link('新增子項目', Yii::app()->createUrl("admin/subtechnology/create"), 		
			array('id'=>'filterBtn', 'class' => 'btn_blue_send')
		);?>
	</div>

	<div style="margin-top:20px;">
		數據指標：
	<?php 
		$TechnologyOpts = CHtml::listData($technologys, 'id', 'name');

		echo CHtml::dropDownList('TechnologySel', 'TechnologyVal', $TechnologyOpts, 
			array(
				'class'=>'span2',
				'style' => (count($TechnologyOpts)<=0)?'display: none;':'display:inline-block;',
				'ajax' => array(
					'type' => 'GET',
					'url'=>CController::createUrl('SubTechnology/AjaxGetSubTechnology'),
					'update' => '#SubTechnologyList',
					'data' => array(
						"Technology"=>'js:$("#TechnologySel").val()',
					),
				)
			)
		); 
	?>
	</div>

	<div id="SubTechnologyList" class="SubTechnologyContent" style="">
	<?php $this->renderPartial('_Table', array(
			'data' => $data,
		)); ?>
	</div>
</div>
<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScript('autoLoad', "
	$('#filterBtn').click(function(event){
		var Technology = $('#TechnologySel').val();
		window.location.href='subtechnology/create/Technology/'+Technology; 
		return false;
	});
");
// $cs->registerScriptFile($moduleAssetUrl.'/js/productmanage/main.js', CClientScript::POS_END);
?>