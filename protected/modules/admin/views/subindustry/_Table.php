<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'industry-grid',
    'dataProvider'=> $data,
    // 'filter' => $model,
    'columns'=>array(
    	// array(            // display 'create_time' using an expression
    		// 'header' => '編號',
    		// 'name' => 'id',
    		// 'htmlOptions' => array('width'=> '50px'),
        // ),
        array(            // display 'create_time' using an expression
            'header'=>'分類名稱',
            'name'=>'Industry.name',
            'htmlOptions' => array('style'=>'text-align:center;'),
	    ),
	    array(            // display 'create_time' using an expression
            'header'=>'分類名稱',
            'name'=>'name',
            'htmlOptions' => array('style'=>'text-align:center;'),
        ),
       array(            // display 'create_time' using an expression
            'header'=>'更新時間',
            'name'=>'Update_at',
            'htmlOptions' => array('width'=> '150px', 'style'=>'text-align:center;'),
        ),
        array(
        	'class'=>'CButtonColumn',
        	'template'=>'{view}{update}{delete}',
        	'buttons'=>array(
        		'view' => array(
        			'url' => 'Yii::app()->createUrl("admin/SubIndustry/view", array("id"=>$data->id))',
        			'options' => array('class'=>'test')
        		),
        		'update' => array(
        			'url' => 'Yii::app()->createUrl("admin/SubIndustry/update", array("id"=>$data->id))',
        			'options' => array('class'=>'test')
        		),
        		'delete' => array(
        			'url' => 'Yii::app()->createUrl("admin/SubIndustry/delete", array("id"=>$data->id))',
        			// 'options' => array('class'=>'test'),
        		),
   			)
        ),
    ),
));
?>