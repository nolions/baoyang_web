<style type="text/css">
	#industry-grid input{
		width: 95%;
	}

	.filters  td{
		text-align: center;
	}
</style>
<?php
$this->breadcrumbs=array(
	'產業子分類管理',
);

$this->widget('application.widgets.BlueTab.BlueTab', array(
	'items' => array(
				'產業分類'=>$this->createUrl('industry/'),
				'子分類'=>$this->createUrl('subindustry/'),
				),
	'on' => 1,
));
?>

<div>
	<div style="width:120px;">
		<?=CHtml::link('新增子分類', Yii::app()->createUrl("admin/subindustry/create"), 		
			array('id'=>'filterBtn', 'class' => 'btn_blue_send')
		);?>
	</div>

	<div style="margin-top:20px;">
		產業類別：
	<?php 
		$IndustryOpts = CHtml::listData($industrys, 'id', 'name');

		echo CHtml::dropDownList('IndustrySel', 'IndustryVal', $IndustryOpts, 
			array(
				'class'=>'span2',
				'style' => (count($IndustryOpts)<=0)?'display: none;':'display:inline-block;',
				'ajax' => array(
					'type' => 'GET',
					'url'=>CController::createUrl('SubIndustry/AjaxGetSubIndustry'),
					'update' => '#SubIndustryList',
					'data' => array(
						"Industry"=>'js:$("#IndustrySel").val()',
					),
				)
			)
		); 
	?>
	</div>

	<div id="SubIndustryList" class="SubIndustryContent" style="">
	<?php $this->renderPartial('_Table', array(
			'data' => $data,
		)); ?>
	</div>
</div>
<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScript('autoLoad', "
	$('#filterBtn').click(function(event){
		var Industry = $('#IndustrySel').val();
		window.location.href='subindustry/create/Industry/'+Industry; 
		return false;
	});
");
// $cs->registerScriptFile($moduleAssetUrl.'/js/productmanage/main.js', CClientScript::POS_END);
?>