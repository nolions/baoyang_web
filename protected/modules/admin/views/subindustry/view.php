<?php
$this->breadcrumbs=array(
	'產業子分類管理' => array('subindustry/'),
	$model->id .": " . $model->name ,
);
?>

<div class="">
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			array(
				'label'=>'子產業名稱：',
				'type'=>'raw',
				'value'=>$model->name,
				// 'visible'=> false,
			),
			array(
				'label'=>'隸屬產業名稱：',
				'type'=>'raw',
				'value'=>$model->Industry->name,
				// 'visible'=> false,
			),
			array(
				'label'=>'更新時間：',
				'type'=>'raw',
				'value'=>$model->Update_at,
				'visible'=> ($model->Update_at == null || $model->Update_at =='')?false:true,
			),
		),
	)); ?>
</div>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
// $cs->registerScriptFile($moduleAssetUrl.'/js/productmanage/main.js', CClientScript::POS_END);
?>