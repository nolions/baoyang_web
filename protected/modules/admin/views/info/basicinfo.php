<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$this->breadcrumbs=array(
	'網站資訊管理-博洋資訊' ,
);

?>

<?php if(Yii::app()->user->hasFlash('contact')): ?>
<div class="flash_success">
	<div class="checked_div">
		<div class="checked_icon" style="width:128px;height:128px;background:url(<?=$assetUrl?>/images/service/ok_icon.png) no-repeat; float:left;"></div>
		<div class="checked_label" style="color:red;padding-top:30px;">
			<h2><?php echo Yii::app()->user->getFlash('contact'); ?></h2>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php endif; ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<?= $form->errorSummary($model, "請解決下列的錯誤"); ?>
	<div class="">
		<table class="ProductInfoTable">
			<thead>
				<tr class="header"><th colspan="3">博洋資訊</th></tr>
				<tr><td></td><td></td></tr>
			</thead>
			<tbody>
				<tr><td></td><td></td><td></td></tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'title', array('style' => 'display:initial;'));?>
					</td>
					<td width="300">
						<?php echo $form->textField($model,'title', array('style' => 'width:95%;'));?>
					</td>
					<td width="200">
						<?php echo $form->error($model,'title'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'tel',array('style' => 'display:initial;'));?>
					</td>
					<td width="300">
						<?php echo $form->textField($model,'tel', array('style' => 'width:95%;'));?>
					</td>
					<td width="200">
						<?=$form->error($model,'tel'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'fax',array('style' => 'display:initial;'));?>
					</td>
					<td width="300">
						<?php echo $form->textField($model,'fax', array('style' => 'width:95%;'));?>
					</td>
					<td width="200">
						<?php echo $form->error($model,'fax'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'email',array('style' => 'display:initial;'));?>
					</td>
					<td width="300">
						<?php echo $form->textField($model,'email', array('style' => 'width:95%;'));?>
					</td>
					<td width="200">
						<?php echo $form->error($model,'email'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'zip',array('style' => 'display:initial;'));?>
					</td>
					<td width="300">
						<?php echo $form->textField($model,'zip', array('style' => 'width:95%;'));?>
					</td>
					<td width="200">
						<?php echo $form->error($model,'zip'); ?>
					</td>
				</tr>
				<tr>
					<td width="100">
						<?php echo $form->labelEx($model,'address',array('style' => 'display:initial;'));?>
					</td>
					<td width="300">
						<?php echo $form->textField($model,'address', array('style' => 'width:95%;'));?>
					</td>
					<td width="200">
						<?php echo $form->error($model,'address'); ?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="buttons" style="">
		<?php echo CHtml::submitButton('儲存', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form end-->

<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/form.css');
$cs->registerCssFile($assetUrl.'/css/main.css');
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScriptFile($moduleAssetUrl.'/js/main.js', CClientScript::POS_END);
?>