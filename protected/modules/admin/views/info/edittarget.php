<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$this->breadcrumbs=array(
	'網站資訊管理-博洋願景' ,
);
?>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<?php endif; ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

<div>
	<div>
		<?= $form->textArea($model, 'content', array('rows'=>6, 'cols'=>50,'id'=>'editor1', 'class'=>''));?>
	</div>
	<br>
	<div class="buttons" style="">
		<?php echo CHtml::submitButton('儲存', array(
			'class'=>'btn_blue_send', 'style'=>'width:100px;display:initial;'
		)); ?>
	</div>
</div>
<?php $this->endWidget(); ?>

<?php
$assetUrl = publish('application.assets');
$moduleAssetUrl = publish('application.modules.admin.assets');

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetUrl.'/css/main.css');
$cs->registerCssFile($assetUrl.'/css/button.css');
$cs->registerCssFile($moduleAssetUrl.'/css/_table.css');
$cs->registerScriptFile($moduleAssetUrl.'/js/main.js', CClientScript::POS_END);
$cs->registerScriptFile($moduleAssetUrl.'/vendors/ckeditor/ckeditor.js', CClientScript::POS_END);
// $cs->registerScriptFile($moduleAssetUrl.'/vendors/ckfinder/ckfinder.js', CClientScript::POS_END);
$cs->registerScript('', "
	CKEDITOR.replace( 'editor1',
	{
		toolbar :'Custom',
	});
", CClientScript::POS_END);
?>