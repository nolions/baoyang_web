<?php
$this->breadcrumbs=array(
	'客戶意見' => array('index'),
	$model->id .": " . $model->title ,
);
?>
<div>
	<div class="">
		<div class="bascinfo_header header"></div>
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				array(
					'label'=>'客戶姓名：',
					'type'=>'raw',
					'value'=>$model->name,
					// 'visible'=> false,
				),
				array(
					'label'=>'聯絡電話：',
					'type'=>'raw',
					'value'=>$model->tel,
					// 'visible'=> ($model->Product_index == null || $model->Product_index =='')?false:true,
				),
				array(
					'label'=>'Email：',
					'type'=>'raw',
					'value'=>$model->email,
					// 'visible'=> ($model->ContrastProduct == null || $model->ContrastProduct =='')?false:true,
				),
				array(
					'label'=>'詢問事項',
					'type'=>'raw',
					'value'=>$model->title,
					// 'visible'=> ($model->Product_type == null || $model->Product_type =='')?false:true,
				),
				array(
					'label'=>'詳細情況：',
					'type'=>'raw',
					'value'=>$model->content,
					// 'visible'=> ($model->Exterior == null || $model->Exterior =='')?false:true,
				),
				array(
					'label'=>'詢問時間：',
					'type'=>'raw',
					'value'=>$model->Create_at,
					// 'visible'=> ($model->Update_at == null || $model->Update_at =='')?false:true,
				),
			),
		)); ?>
	</div>
</div>

<?php

?>