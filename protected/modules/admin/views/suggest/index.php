<?php
$this->breadcrumbs=array(
	'客戶意見列表',
);
?>
<style type="text/css">
	#products-grid td{
		text-align: center;
	}
	#products-grid input{
		width: 94%;
	}
</style>
<div>
	<div class="IndustryContent" style="">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'products-grid',
	    'dataProvider'=> $data,
	    'filter' => $model,
	    'columns'=>array(
	    	//array(            // display 'create_time' using an expression
	        //   'header'=>'#',
	        //    'name'=>'id',
	        //    'filter'=>false,
	        //    'htmlOptions' => array('style'=>'text-align: center; width:40px;'),
	        //),
	    	array(            // display 'create_time' using an expression
	            'header'=>'客戶姓名',
	            'name'=>'name',
	            'htmlOptions' => array('style'=>'text-align: center; width:100px;'),
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'聯絡電話',
	            'name'=>'tel',
	            'htmlOptions' => array('style'=>'text-align: center; width:80px;'),
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'Email',
	            'name'=>'email',
	            'htmlOptions' => array('style'=>'text-align: center; width:200px;'),
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'詢問事項',
	            'name'=>'title',
	            'htmlOptions' => array('style'=>'text-align: left;'),
	        ),
	        array(            // display 'create_time' using an expression
	            'header'=>'詢問時間',
	            'name'=>'Create_at',
	            'filter'=>false,
	            'htmlOptions' => array('style'=>'text-align: center; width:140px;'),
	        ),
	         array(
	        	'class'=>'CButtonColumn',
	        	'template'=>'{view}',
	        	'buttons'=>array(
	        		'view' => array(
	        			'url' => 'Yii::app()->createUrl("admin/suggest/view", array("id"=>$data->id))',
	        			'options' => array('class'=>'test')
	        		),
    			),
    			// 'htmlOptions' => array('style'=>'text-align: center; width:10px;'),
	        ),
	    ),
	)); ?>
	</div>
</div>