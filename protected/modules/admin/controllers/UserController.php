<?php

class UserController extends Controller {
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array('allow',
				'actions'=>array('login')
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('logout', 'setnewpassword'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionLogin() {
		if(!Yii::app()->user->isGuest)
			$this->redirect($this->createUrl('/admin'));

		Yii::import('application.models.forms.LoginForm');

		$model = new LoginForm;
		$formId = 'login-form';

		//var_dump(Yii::app()->user->isGuest );
		if(isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				
				$this->redirect($this->createUrl('/admin'));
			}
		}

		// display the login form
		$this->render('login',array(
			'model' => $model,
		));
	}

	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect($this->createUrl('/admin'));
	}

	public function actionSetnewpassword() {
		Yii::import('application.models.forms.SetNewPasswordForm');

		$model = new SetNewPasswordForm;
		$formId = 'forgotpassword-form';

		if (isset($_POST['SetNewPasswordForm']) ){
			$model->attributes = $_POST['SetNewPasswordForm'];
			if($model->validate() && $model->setNewPassword()){
				$this->actionLogout();
			}
		}

		// display the forgotpassword form
		$this->render('setnewpassword',array(
			'model' => $model,
		));
	}
}