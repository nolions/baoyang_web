<?php

class TypeController extends Controller
{
	private $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create', 'view', 'update', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$model =new Type('search');
		$model->unsetAttributes(); 
        	
        	if(isset($_GET['Type']))
            	$model->attributes=$_GET['Type'];
		

        $this->render('index', array(
			'model'=>$model,
			'data'=>$model->search(),
		));
	}

	public function actionCreate()
	{
		$model = new Type();

		if(isset($_POST['Type']))
		{
			$model->attributes=$_POST['Type'];

			if($model->validate())
			{
				$model->Create_at = date('Y-m-d H:i:s');
				$model->Update_at = $model->Create_at;

				if($model->save())
				{
					//
					//Store Product Apply
					//
					if (isset($_POST['Industrys'])){
						$this->StoreSubIndustryforType($model->id, $_POST['Industrys']);
					}

					//call back view page
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$Industrys = Industry::model()->findAll();

		$this->render('create', array(
			'model' => $model,
			'Industrys' => $Industrys,
		));
	}

	public function actionView()
	{
		$Industrys = Industry::model()->findAll();

		$Applys = SubIndustryforType::model()->findAll('Type_id=:Type_id', array(
					':Type_id'=>$_GET['id']));
		$ApplyIds = array();
		foreach ($Applys as $key => $value) {
			$ApplyIds[] = $value['SubIndustry_id'];
		}


		$this->render('view', array(
			'model' => $this->loadModel(),
			'Industrys' => $Industrys,
			'IndustrysApplys' => $ApplyIds,
		));
	}

	public function actionUpdate()
	{
		$model=$this->loadModel();

		if(isset($_POST['Type']))
		{
			$model->attributes=$_POST['Type'];

			if($model->validate())
			{
				$model->Update_at = date('Y-m-d H:i:s');

				if($model->save())
				{
					$this->DeleteSubIndustryforType($_GET['id']);

					//
					//Store Product Apply
					//
					
					if (isset($_POST['Industrys'])){
						$this->StoreSubIndustryforType($_GET['id'], $_POST['Industrys']);
					}

					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$Industrys = Industry::model()->findAll();

		$Applys = SubIndustryforType::model()->findAll('Type_id=:Type_id', array(
					':Type_id'=>$_GET['id']));
		$ApplyIds = array();
		foreach ($Applys as $key => $value) {
			$ApplyIds[] = $value['SubIndustry_id'];
		}

		$this->render('update', array(
			'model'=>$model,
			'Industrys' => $Industrys,
			'IndustrysApplys' => $ApplyIds,
		));
	}

	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			$this->DeleteSubIndustryforType($_GET['id']);

			$this->DeleteSubTypes($_GET['id']);
			
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	private function loadModel(){
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Type::model()->findbyPk(array('id'=>$_GET['id']));
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}

		return $this->_model;
	}

	private function StoreSubIndustryforType($TypeId, $Industrys)
	{
		foreach ($Industrys as $key => $value) {
			$SubIndustryforType = new SubIndustryforType();
			$SubIndustryforType->SubIndustry_id = $value;
			$SubIndustryforType->Type_id = $TypeId;
			$SubIndustryforType->Update_at = date('Y-m-d H:i:s');
			$SubIndustryforType->save(); //Store Apply Industrys
		}
	}

	private function DeleteSubIndustryforType($TypeId)
	{
		$SubIndustryforTypeModels = SubIndustryforType::model()->findAll(
			'Type_id=:Type_id', 
			array(':Type_id'=>$TypeId));

		foreach ($SubIndustryforTypeModels as $key => $value) {
			$SubIndustryforType = SubIndustryforType::model()->findbyPk(array(
				'SubIndustry_id'=>$value['SubIndustry_id'], 
				'Type_id'=>$TypeId,
			));

			$SubIndustryforType->delete();
		}
	}

	private function DeleteSubTypes($TypeId)
	{
		$SubTypes = SubType::model()->findAll('Type_id=:Type_id', array(
				':Type_id'=>$TypeId));

		foreach ($SubTypes as $key => $value) {
			$SubType = SubType::model()->findbyPk(array('id'=>$value['id']));

			$this->DeleteSubTypeforTechnology($value['id']);

			$this->DeleteProductSortType($value['id']);
			
			$SubType->delete();
		}
	}

	private function DeleteSubTypeforTechnology($SubType)
	{
		$SubTypeforTechnologyModels = SubTypeforTechnology::model()->findAll(
			'SubType_id=:SubType_id', 
			array(':SubType_id'=>$SubType));

		foreach ($SubTypeforTechnologyModels as $key => $value) {
			$SubTypeforTechnology = SubTypeforTechnology::model()->findbyPk(array(
				'SubType_id'=>$SubType, 
				'Technology_id'=>$value['Technology_id'],
			));

			$SubTypeforTechnology->delete();
		}
	}

	private function DeleteProductSortType($SubType)
	{
		$ProductSortModels = ProductSort::model()->findAll(
			'SubType_id=:SubType_id', 
			array(
				':SubType_id'=>$SubType
			)
		);

		foreach ($ProductSortModels as $key => $value) {
			$model = ProductSort::model()->findbyPk(array(
				'ProductItem_id'=>$value['ProductItem_id'], 
				'SubType_id'=>$SubType,
			));
			
			$model->delete();
		}
	}
}
