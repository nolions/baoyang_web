<?php
class SubindustryController extends Controller
{
	private $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create', 'view', 'update', 'delete', 'ajaxgetsubindustry'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex($IndustryId=null)
	{
		$IndustryModel = new Industry();
		$Industrys = $IndustryModel->findAll();

		$IndustryId = ($IndustryId == null)?$IndustryModel->getLastActiveRow()->id:$IndustryId;


		$model =new SubIndustry('search');
		$model->unsetAttributes(); 
        	
        if(isset($_GET['SubIndustry']))
        	$model->attributes=$_GET['SubIndustry'];
		

        $this->render('index', array(
			'model'=>$model,
			'industrys'=>$Industrys,
			'IndustryId'=>$IndustryId,
			'data'=>$model->searchbyIndustry($IndustryId),
		));
	}

	public function actionAjaxGetSubIndustry($Industry = 0)
	{
		$model =new SubIndustry('search');
		$model->unsetAttributes(); 
        	
        if(isset($_GET['SubIndustry']))
        	$model->attributes=$_GET['SubIndustry'];

		$this->renderPartial('_Table', array(
			'data'=>$model->searchbyIndustry($Industry),
		));
	}

	public function actionCreate($Industry=null)
	{
		$model = new SubIndustry();

		if(isset($_POST['SubIndustry']))
		{
			$model->attributes=$_POST['SubIndustry'];

			$model->Create_at = date('Y-m-d H:i:s');
			$model->Update_at = $model->Create_at;

			if($model->save())
			{
				//call back view page
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$IndustryModel = new Industry();
		
		$Industrys = $IndustryModel->findAll();
		//Set radioButtonList default id
		$model->Industry_id = ($Industry==null)?$IndustryModel->getLastActiveRow()->id:$Industry;
		
		$this->render('create', array(
			'model' => $model,
			'Industrys' => $Industrys,
		));
	}

	public function actionView()
	{
		$this->render('view', array(
			'model' => $this->loadModel(),
		));
	}

	public function actionUpdate()
	{
		$model=$this->loadModel();

		if(isset($_POST['Industry']))
		{
			$model->attributes=$_POST['Industry'];

			if($model->validate())
			{
				$model->Update_up = date('Y-m-d H:i:s');

				if($model->save())
				{
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$Industrys = Industry::model()->findAll();

		$this->render('update', array(
			'model'=>$model,
			'Industrys' => $Industrys,
		));
	}

	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			//
			$this->DeleteSubIndustryforType($_GET['id']);

			//
			$this->DeleteProductApplyIndustry($_GET['id']);

			//
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	private function DeleteSubIndustryforType($SubIndustry)
	{
		$SubIndustryforTypeModels = SubIndustryforType::model()->findAll(
			'SubIndustry_id=:SubIndustry_id', 
			array(':SubIndustry_id'=>$SubIndustry));

		foreach ($SubIndustryforTypeModels as $key => $value) {
			$SubIndustryforType = SubIndustryforType::model()->findbyPk(array(
				'SubIndustry_id'=>$SubIndustry, 
				'Type_id'=>$value['Type_id'],
			));

			$SubIndustryforType->delete();
		}
	}

	private function DeleteProductApplyIndustry($SubIndustry)
	{
		$ProductItemApplyModels = ProductItemApply::model()->findAll(
			'Industry_id=:Industry_id', 
			array(
					':Industry_id'=>$SubIndustry
			));

		foreach ($ProductItemApplyModels as $key => $value) {
			$model = ProductItemApply::model()->findbyPk(array(
				'ProductItem_id'=>$value['ProductItem_id'], 
				'Industry_id'=>$SubIndustry,
			));
			$model->delete();
		}
	}

	private function loadModel(){
		if($this->_model===null)
		{
			if(isset($_GET['id']))
			
				$this->_model=SubIndustry::model()->findbyPk(array('id'=>$_GET['id']));
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}

		return $this->_model;
	}
}
