<?php

class SuggestController extends Controller
{
	private $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{
		$model =new Opinion('search');
		
		$model->unsetAttributes();  
        // print_r($model->search());

        if(isset($_GET['Opinion']))
            $model->attributes=$_GET['Opinion'];

		$this->render('index', array(
			'model'=>$model,
			'data'=>$model->search(),
		));
	}

	public function actionView()
	{
		$this->render('view', array(
			'model' => $this->loadModel(),
		));
	}

	private function loadModel(){
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Opinion::model()->findbyPk(array('id'=>$_GET['id']));
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}

		return $this->_model;
	}
}
