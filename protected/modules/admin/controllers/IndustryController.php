<?php
class IndustryController extends Controller
{
	private $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create', 'view', 'update', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$model =new Industry('search');
		$model->unsetAttributes(); 
        	
        	if(isset($_GET['Industry']))
            	$model->attributes=$_GET['Industry'];
		

        $this->render('index', array(
			'model'=>$model,
			'data'=>$model->search(),
		));
	}

	public function actionCreate()
	{
		$model = new Industry();

		if(isset($_POST['Industry']))
		{
			$model->attributes=$_POST['Industry'];

			$model->Create_up = date('Y-m-d H:i:s');
			$model->Update_up = $model->Create_up;

			if($model->save())
			{
				//call back view page
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	public function actionView()
	{
		$this->render('view', array(
			'model' => $this->loadModel(),
		));
	}

	public function actionUpdate()
	{
		$model=$this->loadModel();

		if(isset($_POST['Industry']))
		{
			$model->attributes=$_POST['Industry'];

			if($model->validate())
			{
				$model->Update_up = date('Y-m-d H:i:s');

				if($model->save())
				{
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$this->render('update', array(
			'model'=>$model,
		));
	}

	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			//
			//Delete SubIndustrys based on  Industry Id
			//
			$this->DeleteSubIndustrys($_GET['id']);
			
			//
			//Delete Industry
			//
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	private function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Industry::model()->findbyPk(array('id'=>$_GET['id']));
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}

		return $this->_model;
	}

	private function DeleteSubIndustrys($IndustryId)
	{
		$SubIndustrys = SubIndustry::model()->findAll('Industry_id=:Industry_id', array(
				':Industry_id'=>$IndustryId));

		foreach ($SubIndustrys as $key => $value) {
			$SubIndustry = SubIndustry::model()->findbyPk(array('id'=>$value['id']));
			
			//
			$this->DeleteSubIndustryforType($value['id']);

			//
			$this->DeleteProductApplyIndustry($value['id']);

			$SubIndustry->delete();
		}
	}

	private function DeleteSubIndustryforType($SubIndustry)
	{
		$SubIndustryforTypeModels = SubIndustryforType::model()->findAll(
			'SubIndustry_id=:SubIndustry_id', 
			array(':SubIndustry_id'=>$SubIndustry));

		foreach ($SubIndustryforTypeModels as $key => $value) {
			$SubIndustryforType = SubIndustryforType::model()->findbyPk(array(
				'SubIndustry_id'=>$SubIndustry, 
				'Type_id'=>$value['Type_id'],
			));

			$SubIndustryforType->delete();
		}
	}

	private function DeleteProductApplyIndustry($SubIndustry)
	{
		$ProductItemApplyModels = ProductItemApply::model()->findAll(
			'Industry_id=:Industry_id', 
			array(
					':Industry_id'=>$SubIndustry
			));

		foreach ($ProductItemApplyModels as $key => $value) {
			$model = ProductItemApply::model()->findbyPk(array(
				'ProductItem_id'=>$value['ProductItem_id'], 
				'Industry_id'=>$SubIndustry,
			));
			$model->delete();
		}
	}
}
