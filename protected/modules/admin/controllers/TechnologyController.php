<?php

class TechnologyController extends Controller
{
	private $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create', 'view', 'update', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$model =new Technology('search');
		$model->unsetAttributes(); 
        	
        	if(isset($_GET['Technology']))
            	$model->attributes=$_GET['Technology'];
		
            
        $this->render('index', array(
			'model'=>$model,
			'data'=>$model->search(),
		));
	}

	public function actionCreate()
	{
		$model = new Technology();

		if(isset($_POST['Technology']))
		{
			$model->attributes=$_POST['Technology'];

			if($model->validate())
			{
				$model->Create_at = date('Y-m-d H:i:s');
				$model->Update_at = $model->Create_at;

				if($model->save())
				{
					//
					//Store Technology for Sub Type
					//
					if (isset($_POST['SubType'])){
						$this->StoreSubTypeforTechnology($model->id, $_POST['SubType']);
					}

					//call back view page
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$Types = Type::model()->findAll();

		$this->render('create', array(
			'model' => $model,
			'Types' => $Types,
		));
	}

	public function actionView()
	{
		$Types = Type::model()->findAll();

		$Sorts = SubTypeforTechnology::model()->findAll('Technology_id=:Technology_id', array(
					':Technology_id'=>$_GET['id']));
		$SortIds = array();
		foreach ($Sorts as $key => $value) {
			$SortIds[] = $value['SubType_id'];
		}


		$this->render('view', array(
			'model' => $this->loadModel(),
			'Types' => $Types,
			'TypeSortIds' => $SortIds,
		));
	}

	public function actionUpdate()
	{
		$model=$this->loadModel();

		if(isset($_POST['Technology']))
		{
			$model->attributes=$_POST['Technology'];

			if($model->validate())
			{
				$model->Update_at = date('Y-m-d H:i:s');

				if($model->save())
				{
					//
					//Delete Technology for Sub Type
					//
					$this->DeleteSubTypeforTechnology($_GET['id']);

					//
					//Store Technology for Sub Type
					//
					if (isset($_POST['SubType'])){
						$this->StoreSubTypeforTechnology($_GET['id'], $_POST['SubType']);
					}

					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$Types = Type::model()->findAll();

		$Sorts = SubTypeforTechnology::model()->findAll('Technology_id=:Technology_id', array(
					':Technology_id'=>$_GET['id']));
		$SortIds = array();
		foreach ($Sorts as $key => $value) {
			$SortIds[] = $value['SubType_id'];
		}

		$this->render('update', array(
			'model'=>$model,
			'Types' => $Types,
			'TypeSortIds' => $SortIds,
		));
	}

	public function actionDelete()
	{

		if(Yii::app()->request->isPostRequest)
		{
			$this->DeleteSubTypeforTechnology($_GET['id']);

			$this->DeleteSubTechnology($_GET['id']);

			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			//if(!isset($_GET['ajax']))
			//	$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	private function loadModel(){
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Technology::model()->findbyPk(array('id'=>$_GET['id']));
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}

		return $this->_model;
	}

	private function StoreSubTypeforTechnology($TechnologyId, $SubTypes)
	{
		foreach ($SubTypes as $key => $value) {
			$SubTypeforTechnology = new SubTypeforTechnology();
			$SubTypeforTechnology->SubType_id = $value;
			$SubTypeforTechnology->Technology_id = $TechnologyId;
			$SubTypeforTechnology->Update_at = date('Y-m-d H:i:s');
			$SubTypeforTechnology->save(); //Store SubType for Technology
		}
	}

	private function DeleteSubTypeforTechnology($TechnologyId)
	{
		$SubTypeforTechnologyModels = SubTypeforTechnology::model()->findAll(
			'Technology_id=:Technology_id', 
			array(':Technology_id'=>$TechnologyId));

		foreach ($SubTypeforTechnologyModels as $key => $value) {
			$SubTypeforTechnology = SubTypeforTechnology::model()->findbyPk(array(
				'SubType_id'=>$value['SubType_id'], 
				'Technology_id'=>$TechnologyId,
			));

			$SubTypeforTechnology->delete();
		}
	}

	private function DeleteSubTechnology($TechnologyId)
	{
		$SubTechnologys = SubTechnology::model()->findAll('Technology_id=:Technology_id', array(
				':Technology_id'=>$TechnologyId));

		foreach ($SubTechnologys as $key => $value) {
			$SubTechnology = SubTechnology::model()->findbyPk(array('id'=>$value['id']));
			
			$this->DeleteProductTechnologyIndex($value['id']);

			$SubTechnology->delete();
		}
	}

	private function DeleteProductTechnologyIndex($SubTechnology)
	{
		$ProductTechnologyIndexModels = ProductIndex::model()->findAll(
			'SubTechnology_id=:SubTechnology_id', 
			array(
				':SubTechnology_id'=>$SubTechnology
			));

		foreach ($ProductTechnologyIndexModels as $key => $value) {
			$model = ProductIndex::model()->findbyPk(array(
				'ProductItem_id'=>$value['ProductItem_id'], 
				'SubTechnology_id'=>$SubTechnology,
			));
			
			$model->delete();
		}
	}
}