<?php
class SubtypeController extends Controller
{
	private $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create', 'view', 'update', 'delete', 'ajaxgetsubtype'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex($TypeId=null)
	{
		$TypeModel = new Type();
		$TypeId = ($TypeId == null)?$TypeModel->getLastActiveRow()->id:$TypeId;

		$model =new SubType('search');
		$model->unsetAttributes(); 
        	
        if(isset($_GET['SubIndustry']))
        	$model->attributes=$_GET['SubIndustry'];
		
        $this->render('index', array(
			'model'=>$model,
			'types'=>$TypeModel->findAll(),
			//'TypeId'=>$TypeId,
			'data'=>$model->searchbyType($TypeId),
		));
	}

	public function actionAjaxGetSubType($Type = 0)
	{
		$model =new SubType('search');
		$model->unsetAttributes(); 
        	
        if(isset($_GET['SubType']))
        	$model->attributes=$_GET['SubType'];

		$this->renderPartial('_Table', array(
			'data'=>$model->searchbyType($Type),
		));
	}

	public function actionCreate($Type=null)
	{
		$model = new SubType();

		if(isset($_POST['SubType']))
		{
			$model->attributes=$_POST['SubType'];

			$model->Create_at = date('Y-m-d H:i:s');
			$model->Update_at = $model->Create_at;

			if($model->save())
			{
				//call back view page
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$TypeModel = new Type();
		
		//Set radioButtonList default id
		// $model->Type_id = ($_POST['Type'])?$_POST['Type']:$TypeModel->getLastActiveRow()->id;
		$model->Type_id = ($Type==null)?$TypeModel->getLastActiveRow()->id:$Type;

		$this->render('create', array(
			'model' => $model,
			'Types' => $TypeModel->findAll(),
		));
	}

	public function actionView()
	{
		$this->render('view', array(
			'model' => $this->loadModel(),
		));
	}

	public function actionUpdate()
	{
		$model=$this->loadModel();

		if(isset($_POST['SubType']))
		{
			$model->attributes=$_POST['SubType'];

			if($model->validate())
			{
				$model->Update_at = date('Y-m-d H:i:s');

				if($model->save())
				{
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$this->render('update', array(
			'model'=>$model,
			'Types' => Type::model()->findAll(),
		));
	}

	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			$this->DeleteSubTypeforTechnology($_GET['id']);
			
			$this->DeleteProductSortType($_GET['id']);

			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	private function DeleteSubTypeforTechnology($SubType)
	{
		$SubTypeforTechnologyModels = SubTypeforTechnology::model()->findAll(
			'SubType_id=:SubType_id', 
			array(':SubType_id'=>$SubType));

		foreach ($SubTypeforTechnologyModels as $key => $value) {
			$SubTypeforTechnology = SubTypeforTechnology::model()->findbyPk(array(
				'SubType_id'=>$SubType, 
				'Technology_id'=>$value['Technology_id'],
			));

			$SubTypeforTechnology->delete();
		}
	}

	private function DeleteProductSortType($SubType)
	{
		$ProductSortModels = ProductSort::model()->findAll(
			'SubType_id=:SubType_id', 
			array(
				':SubType_id'=>$SubType
			)
		);

		foreach ($ProductSortModels as $key => $value) {
			$model = ProductSort::model()->findbyPk(array(
				'ProductItem_id'=>$value['ProductItem_id'], 
				'SubType_id'=>$SubType,
			));
			
			$model->delete();
		}
	}

	private function loadModel(){
		if($this->_model===null)
		{
			if(isset($_GET['id']))
			
				$this->_model=SubType::model()->findbyPk(array('id'=>$_GET['id']));
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}

		return $this->_model;
	}
}
