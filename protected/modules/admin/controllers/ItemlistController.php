<?php

class ItemlistController extends Controller
{
	private $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create', 'view', 'update', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		// var_dump( Yii::app()->user->isGuest );
		$model =new ProductItems('search');
		$model->unsetAttributes();  
        	
        if(isset($_GET['ProductItems']))
            $model->attributes=$_GET['ProductItems'];

		$this->render('index', array(
			'model'=>$model,
			'data'=>$model->search(),
		));
	}

	public function actionCreate()
	{
		$model = new ProductItems();

		if(isset($_POST['ProductItems']))
		{
			$model->attributes=$_POST['ProductItems'];
			
			if($model->validate())
			{
				if(isset($_POST['ProductItems']['Target']))
					$model->Target = $_POST['ProductItems']['Target'];
					$model->Technology = $_POST['ProductItems']['Technology'];
					$model->Physical = $_POST['ProductItems']['Physical'];
					$model->Material = $_POST['ProductItems']['Material'];
					$model->Other = $_POST['ProductItems']['Other'];
				
				$model->Create_at = date('Y-m-d H:i:s');
				$model->Update_at = $model->Create_at;

				//Store Production data
				if($model->save())
				{	
					//
					//Store Product Apply
					//
					if (isset($_POST['Industrys']))
						$this->StoreProductApplyIndustry($model->id, $_POST['Industrys']);
						
					//
					//Store Product Sort
					//
					if (isset($_POST['Types'])) 
						$this->StoreProductSortType($model->id, $_POST['Types']);

					//
					//Store Technology Index
					//
					if (isset($_POST['Technology'])) 
						$this->StoreProductTechnologyIndex($model->id, $_POST['Technology']);

					//call back view page
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$Industrys = Industry::model()->findAll();
		$Types = Type::model()->findAll();
		$Technologys = Technology::model()->findAll();
		
		$this->render('create', array(
			'model' => $model,
			'Industrys' => $Industrys,
			'Types' => $Types,
			'Technologys' => $Technologys,
		));
	}

	public function actionView()
	{
		$Industrys = Industry::model()->findAll();
		$Types = Type::model()->findAll();
		$Technologys = Technology::model()->findAll();
		
		$Applys = ProductItemApply::model()->findAll('ProductItem_id=:ProductItem_id', array(
					':ProductItem_id'=>$_GET['id']));
		$ApplyIds = array();
		foreach ($Applys as $key => $value) {
			$ApplyIds[] = $value['Industry_id'];
		}

		$Sorts = ProductSort::model()->findAll('ProductItem_id=:ProductItem_id', array(
					':ProductItem_id'=>$_GET['id']));
		$SortIds = array();
		foreach ($Sorts as $key => $value) {
			$SortIds[] = $value['SubType_id'];
		}

		$Indexs = ProductIndex::model()->findAll('ProductItem_id=:ProductItem_id', array(
					':ProductItem_id'=>$_GET['id']));
		$IndexIds = array();
		foreach ($Indexs as $key => $value) {
			$IndexIds[] = $value['SubTechnology_id'];
		}

		$this->render('view', array(
			'model' => $this->loadModel(),
			'Industrys' => $Industrys,
			'Types' => $Types,
			'Technologys' => $Technologys,
			'IndustrysApplys' => $ApplyIds,
			'ProductTypes' => $SortIds,
			'TechnologyIndexs' => $IndexIds,
		));
	}

	public function actionUpdate()
	{
		$model=$this->loadModel();

		if(isset($_POST['ProductItems']))
		{
			$model->attributes=$_POST['ProductItems'];
			
			if($model->validate())
			{
				if(isset($_POST['ProductItems']['Target']))
					$model->Target = $_POST['ProductItems']['Target'];
					$model->Technology = $_POST['ProductItems']['Technology'];
					$model->Physical = $_POST['ProductItems']['Physical'];
					$model->Material = $_POST['ProductItems']['Material'];
					$model->Other = $_POST['ProductItems']['Other'];
				
				$model->Update_at = date('Y-m-d H:i:s');

				//Store Production data
				if($model->save())
				{
					//
					//Product Apply
					//
					
					//Delete Product Apply
					$this->DeleteProductApplyIndustry($_GET['id']);

					//Store Product Apply
					if (isset($_POST['Industrys'])) 
						$this->StoreProductApplyIndustry($model->id, $_POST['Industrys']);

					//
					//Product Sort
					//
					
					//Delete Product Sort
					$this->DeleteProductSortType($_GET['id']);

					//Store Product Sort
					if (isset($_POST['Types'])) 
						$this->StoreProductSortType($model->id, $_POST['Types']);

					//
					//Technology Index
					//
					
					//Delete Technology Index
					$this->DeleteProductTechnologyIndex($_GET['id']);

					//Store Technology Index
					if (isset($_POST['Technology'])) 
						$this->StoreProductTechnologyIndex($model->id, $_POST['Technology']);

					//call back view page
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$Industrys = Industry::model()->findAll();
		$Types = Type::model()->findAll();
		$Technologys = Technology::model()->findAll();
		
		$Applys = ProductItemApply::model()->findAll('ProductItem_id=:ProductItem_id', array(
					':ProductItem_id'=>$_GET['id']));
		$ApplyIds = array();
		foreach ($Applys as $key => $value) {
			$ApplyIds[] = $value['Industry_id'];
		}

		$Sorts = ProductSort::model()->findAll('ProductItem_id=:ProductItem_id', array(
					':ProductItem_id'=>$_GET['id']));
		$SortIds = array();
		foreach ($Sorts as $key => $value) {
			$SortIds[] = $value['SubType_id'];
		}

		$Indexs = ProductIndex::model()->findAll('ProductItem_id=:ProductItem_id', array(
					':ProductItem_id'=>$_GET['id']));
		$IndexIds = array();
		foreach ($Indexs as $key => $value) {
			$IndexIds[] = $value['SubTechnology_id'];
		}
		
		$this->render('update',array(
			'model'=>$model,
			'Industrys' => $Industrys,
			'Types' => $Types,
			'Technologys' => $Technologys,
			'IndustrysApplys' => $ApplyIds,
			'ProductTypes' => $SortIds,
			'TechnologyIndexs' => $IndexIds,
		));
	}

	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			 
			$this->DeleteProductApplyIndustry($_GET['id']);

			$this->DeleteProductSortType($_GET['id']);

			$this->DeleteProductTechnologyIndex($_GET['id']);
			
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	private function loadModel(){
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=ProductItems::model()->findbyPk(array('id'=>$_GET['id']));
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}

		return $this->_model;
	}

	private function StoreProductApplyIndustry($ProductId, $IndustryApplys)
	{
		foreach ($IndustryApplys as $key => $value) {
			$ProductItemApply = new ProductItemApply();
			$ProductItemApply->ProductItem_id = $ProductId;
			$ProductItemApply->Industry_id = $value;
			$ProductItemApply->Update_at = date('Y-m-d H:i:s');
			$ProductItemApply->save(); //Store Apply Industrys
		}
	}

	private function StoreProductSortType($ProductId, $SortTypes)
	{
		foreach ($SortTypes as $key => $value) {
			$ProductSort = new ProductSort();
			$ProductSort->ProductItem_id = $ProductId;
			$ProductSort->SubType_id = $value;
			$ProductSort->Update_at = date('Y-m-d H:i:s');
			$ProductSort->save(); //Store Apply Industrys
		}
	}

	private function StoreProductTechnologyIndex($ProductId, $TechnologyIndexs)
	{
		foreach ($TechnologyIndexs as $key => $value) {
			$ProductIndex = new ProductIndex();
			$ProductIndex->ProductItem_id = $ProductId;
			$ProductIndex->SubTechnology_id = $value;
			$ProductIndex->Update_at = date('Y-m-d H:i:s');
			$ProductIndex->save(); //Store Technology Index
		}
	}

	/**
	 * Batch delete Industry Apply about Product from ProductApply Table
	 * @param int $ProductId Product item' id value
	 */
	private function DeleteProductApplyIndustry($ProductId)
	{
		$ProductItemApplyModels = ProductItemApply::model()->findAll('ProductItem_id=:ProductItem_id', array(
					':ProductItem_id'=>$ProductId));

		foreach ($ProductItemApplyModels as $key => $value) {
			$model = ProductItemApply::model()->findbyPk(array(
				'ProductItem_id'=>$value['ProductItem_id'], 
				'Industry_id'=>$value['Industry_id'],
			));
			$model->delete();
		}
	}

	/**
	 * Batch delete Sort Type about Product from ProductSort Table
	 * @param int $ProductId Product item' id value
	 */
	private function DeleteProductSortType($ProductId)
	{
		$ProductSortModels = ProductSort::model()->findAll('ProductItem_id=:ProductItem_id', array(
					':ProductItem_id'=>$ProductId));

		foreach ($ProductSortModels as $key => $value) {
			$model = ProductSort::model()->findbyPk(array(
				'ProductItem_id'=>$value['ProductItem_id'], 
				'SubType_id'=>$value['SubType_id'],
			));
			
			$model->delete();
		}
	}

	private function DeleteProductTechnologyIndex($ProductId)
	{
		$ProductTechnologyIndexModels = ProductIndex::model()->findAll('ProductItem_id=:ProductItem_id', array(
					':ProductItem_id'=>$ProductId));

		foreach ($ProductTechnologyIndexModels as $key => $value) {
			$model = ProductIndex::model()->findbyPk(array(
				'ProductItem_id'=>$value['ProductItem_id'], 
				'SubTechnology_id'=>$value['SubTechnology_id'],
			));
			
			$model->delete();
		}
	}

	// public function isChecked($Display){
	// 	$assetUrl = publish('application.assets');

	// 	$img = ($Display==1)?'uncheck.png':'check.png';
	// 	echo "<img src='".$assetUrl."/images/".$img."'>";
	// }

}
