<?php
class InfoController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('basicinfo', 'editabout', 'edittarget'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionBasicinfo()
	{
		$InfoModel = Info::model()->findbyPk(array('type'=>'info', 'params'=>'siteinfo'));
		$model = new BasicInfoForm();

		if(isset($_POST['BasicInfoForm']))
		{
			$model->attributes=$_POST['BasicInfoForm'];
			
			if($model->validate()){
				$info = array(
						'title'=> $_POST['BasicInfoForm']['title'],
						'tel'=> $_POST['BasicInfoForm']['tel'],
						'fax'=> $_POST['BasicInfoForm']['fax'],
						'email'=> $_POST['BasicInfoForm']['email'],
						'zip'=> $_POST['BasicInfoForm']['zip'],
						'address'=> $_POST['BasicInfoForm']['address'],
					);

				$InfoModel->data = CJSON::encode($info);
				$InfoModel->Update_at = date('Y-m-d H:i:s');

				$InfoModel->save();

				// print_r($InfoModel);
				
				Yii::app()->user->setFlash(
					'contact',
					'網站基本資訊儲存完成'
				);
				
				$this->refresh(); 
			}
			else {
				// $error = $model->getErrors();
				// $error = '系統錯誤！';
			}
		}

		$info = CJSON::decode($InfoModel->data);
		$model->title = $info['title'];
		$model->tel = $info['tel'];
		$model->fax = $info['fax'];
		$model->email = $info['email'];
		$model->zip = $info['zip'];
		$model->address = $info['address'];


		$this->render('basicinfo', array(
			'model' => $model
		));
	}

	public function actionEditabout()
	{
		$model = new AboutForm();

		$aboutModel = Info::model()->findbyPk(array('type'=>'info', 'params'=>'aboutinfo'));

		if(isset($_POST['AboutForm']))
		{
			$model->attributes=$_POST['AboutForm'];
			
			if($model->validate()){
				$info = array(
						'content'=> $_POST['AboutForm']['content'],
					);

				$aboutModel->data = CJSON::encode($info);
				$aboutModel->Update_at = date('Y-m-d H:i:s');

				$aboutModel->save();
				
				// print_r($aboutModel);
				
				Yii::app()->user->setFlash(
					'contact',
					'網站基本資訊儲存完成'
				);
				
				$this->refresh(); 
			}
			else {
				// $error = $model->getErrors();
				// $error = '系統錯誤！';
			}
		}

		$info = CJSON::decode($aboutModel->data);
		$model->content = $info['content'];

		$this->render('editabout', array(
			'model' => $model
		));
	}

	public function actionEdittarget()
	{
		$model = new TargetForm();

		$targetModel = Info::model()->findbyPk(array('type'=>'info', 'params'=>'targetinfo'));

		if(isset($_POST['TargetForm']))
		{
			$model->attributes=$_POST['TargetForm'];
			
			if($model->validate()){
				$info = array(
						'content'=> $_POST['TargetForm']['content'],
					);

				$targetModel->data = CJSON::encode($info);
				$targetModel->Update_at = date('Y-m-d H:i:s');

				$targetModel->save();
				
				// print_r($targetModel);
				
				Yii::app()->user->setFlash(
					'contact',
					'網站基本資訊儲存完成'
				);
				
				$this->refresh(); 
			}
			else {
				// $error = $model->getErrors();
				// $error = '系統錯誤！';
			}
		}

		$info = CJSON::decode($targetModel->data);
		$model->content = $info['content'];

		$this->render('edittarget', array(
			'model' => $model
		));
	}
}

?>