<?php
 
 class BackupController extends Controller
{
	public function actionIndex()
	{
		// $file = $this->_backipDB('baoyang_sort');
	}

	public function actionsortdb(){
		//$file = $this->_backipDB('baoyang_sort');
		$file = $this->_backipDB('baoyangt_baoyang_sort', Yii::app()->dbSort);

		$this->_downloadFile($file);
	}

	public function actionadmindb(){
		//$file = $this->_backipDB('baoyang_admin');
		$file = $this->_backipDB('baoyangt_baoyang_admin', Yii::app()->dbAdmin);

		$this->_downloadFile($file);
	}

	public function actionwebdb(){
		//$file = $this->_backipDB('baoyang_Web');
		$file = $this->_backipDB('baoyangt_baoyang_Web', Yii::app()->dbWeb);

		$this->_downloadFile($file);
	}

	private function _downloadFile($file){
		$dir_path = Yii::getPathOfAlias('webroot');
		$fileName = $dir_path . "/".$file;
		
		if (file_exists($fileName))
			return Yii::app()->getRequest()->sendFile($file, @file_get_contents($fileName));
		else
			throw new CHttpException(404, 'The requested page does not exist.');
	}

	private function _backipDB($DBName, $db){
		Yii::import('application.extensions.yii-dump-db.dumpDB');

		//$dumper = new dumpDB('mysql:host=localhost;dbname='.$DBName, 'root', '1234');
		$dumper = new dumpDB($db);
		$dumper->setRemoveViewDefinerSecurity(TRUE);
		// echo $dumper->getDump(false);	
	 
		$bk_file = 'Backup_'.$DBName.'-'.date('YmdHis').'.sql';
		$fh = fopen($bk_file, 'w') or die("can't open file");
		fwrite($fh, $dumper->getDump(FALSE));
		fclose($fh);

		return $bk_file;
	}
}

?>
