<?php

class SubtechnologyController extends Controller
{
	private $_model;

	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create', 'view', 'update', 'delete','ajaxgetsubtechnology'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex($TechnologyId=null)
	{
		$TechnologyModel = new Technology();
		$TechnologyId = ($TechnologyId == null)?$TechnologyModel->getLastActiveRow()->id:$TechnologyId;

		$model =new SubTechnology('search');
		$model->unsetAttributes(); 
        	
        	if(isset($_GET['SubTechnology']))
            	$model->attributes=$_GET['SubTechnology'];
		

        $this->render('index', array(
			'model'=>$model,
			'technologys'=>$TechnologyModel->findAll(),
			// 'TechnologyId'=>$TechnologyId,
			'data'=>$model->searchbyTechnology($TechnologyId),
		));
	}

	public function actionAjaxGetSubTechnology($Technology = 0)
	{
		$model =new SubTechnology('search');
		$model->unsetAttributes(); 
        	
        if(isset($_GET['SubTechnology']))
        	$model->attributes=$_GET['SubTechnology'];

		$this->renderPartial('_Table', array(
			'data'=>$model->searchbyTechnology($Technology),
		));
	}

	public function actionCreate($Technology=null)
	{
		$model = new SubTechnology();

		if(isset($_POST['SubTechnology']))
		{
			$model->attributes=$_POST['SubTechnology'];

			$model->Create_at = date('Y-m-d H:i:s');
			$model->Update_at = $model->Create_at;

			if($model->save())
			{
				//call back view page
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$TechnologyModel = new Technology();
		
		//Set radioButtonList default id
		// $model->Type_id = ($_POST['Type'])?$_POST['Type']:$TypeModel->getLastActiveRow()->id;
		$model->Technology_id = ($Technology==null)?$TechnologyModel->getLastActiveRow()->id:$Technology;

		$this->render('create', array(
			'model' => $model,
			'Technologys' => $TechnologyModel->findAll(),
		));
	}

	public function actionView()
	{
		$this->render('view', array(
			'model' => $this->loadModel(),
		));
	}

	public function actionUpdate()
	{
		$model=$this->loadModel();

		if(isset($_POST['SubTechnology']))
		{
			$model->attributes=$_POST['SubTechnology'];

			if($model->validate())
			{
				$model->Update_at = date('Y-m-d H:i:s');

				if($model->save())
				{
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$this->render('update', array(
			'model'=>$model,
			'Technologys' => Technology::model()->findAll(),
		));
	}

	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			//
			$this->DeleteProductTechnologyIndex($_GET['id']);

			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	private function DeleteProductTechnologyIndex($SubTechnology)
	{
		$ProductTechnologyIndexModels = ProductIndex::model()->findAll(
			'SubTechnology_id=:SubTechnology_id', 
			array(
				':SubTechnology_id'=>$SubTechnology
			));

		foreach ($ProductTechnologyIndexModels as $key => $value) {
			$model = ProductIndex::model()->findbyPk(array(
				'ProductItem_id'=>$value['ProductItem_id'], 
				'SubTechnology_id'=>$SubTechnology,
			));
			
			$model->delete();
		}
	}

	private function loadModel(){
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=SubTechnology::model()->findbyPk(array('id'=>$_GET['id']));
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}

		return $this->_model;
	}


}
?>