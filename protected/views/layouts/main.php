<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<?php
$assetUrl = publish('application.assets');
$webRoot = Yii::app()->createUrl('/');

$this->renderPartial('//layouts/_header', array());
?>
<body>

<?php
	// top
	$this->renderPartial('//layouts/_top', array());
	// end of top
	// 'Sample post'=>array('post/view', 'id'=>12),
        //'Edit',
?>

<div class="main_container980">
	<div id="breadcrum">
	<?php //if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'homeLink'=>CHtml::link('首頁', Yii::app()->homeUrl),
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php //endif?>
	</div>
</div>
<div class="main_container980" style="background:#ffffff;">
	<div id="main_content">
		<div id="left_content">
		
		<?php if($this->summenu):
				$this->widget('application.widgets.SubMenu.SubMenu', array(
					'items' => $this->summenu));
		endif ?>
		<?php
			// $RecommendMenuArray = array(
			// 	array('name'=>'Facebook', 'url'=>''),
			// 	array('name'=>'Twitter', 'url'=>''),
			// 	array('name'=>'Google', 'url'=>''),
			// );
		
			// $this->widget('application.widgets.SliderRecommendMenu.SliderRecommendMenu', array(
			// 	'title'=>'推薦博洋',
			// 	'items' => $RecommendMenuArray,
			// ));
		?>

			<div class="SubMenu" >
				<div class="SubMenu_Header" style="border-bottom:1px dashed #000;">
					<label class="SildeMenu_Label">QR Code</label>
				</div>				
				<div style="margin-top: 10px; text-align: center; ">
					<?php 
					$this->widget('application.widgets.Qrcode.QrcodeWidget', array(
						//'data' => Yii::app()->params['websiteAddress']. Yii::app()->request->baseUrl, 
						'data' => Yii::app()->createAbsoluteUrl('', array(), 'http'), 
						'width' => '120px',
						'height' => '120px'
					));
					?>
				</div>
			</div>	
		</div>
		<div id="right_content">
			<?= $content; ?>
		</div>

		<div class="clear"></div>
	</div>
	
	<div class="clear"></div>
		
</div><!-- page -->

<!-- footer -->
<?= $this->renderPartial('//layouts/_footer')?>
</body>

</html>
<?php
	$widgetAssetsUrl = publish('application.assets');
	Yii::app()->getClientScript()->registerCssFile($widgetAssetsUrl.'/css/main.css');
?>
