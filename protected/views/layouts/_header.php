<?php
	$assetUrl = publish('application.assets');
	$webRoot = Yii::app()->createUrl('/');
?>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="zh-tw">
	<meta name="language" content="utf-8" />
	<?php if($this->metaTitle): ?>
	<meta name="title" content="<?//=$this->metaTitle?>" />
	<title> <?=$this->metaTitle?> </title>
	<?php endif ?>
	<!-- blueprint CSS framework -->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	
	<!-- <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/mainmenu.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="<?//= Yii::app()->request->baseUrl;?>/css/screen.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	
	<link rel="shortcut icon" href="<?=Yii::app()->createUrl('/')?>/favicon.png"/>
</head>
<?php
	Yii::app()->getClientScript()->registerCssFile($assetUrl.'/css/main.css');
	Yii::app()->getClientScript()->registerCssFile($assetUrl.'/css/form.css');
	Yii::app()->getClientScript()->registerCssFile($assetUrl.'/css/screen.css');
?>


	