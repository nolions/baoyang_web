<?php
$assetUrl = publish('application.assets');
$webRoot = Yii::app()->createUrl('/');
?>
<div id="top">
	<!-- header -->
	<div id="header">
		<div id="top_logo"></div>
	</div>
	<!-- end of header -->
	
	<!-- mainmenu -->
	<div id="mainmenu">
		<div style="width:980px; margin:0 auto;">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'關於博洋', 'url'=>array('/site/index')),
				array('label'=>'產品介紹', 'url'=>array('/product/1')),
				array('label'=>'客戶服務', 'url'=>array('/service/index')),
				// array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				// array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
		</div>
	</div>
	<!-- end of mainmenu -->
</div>
<?php
Yii::app()->getClientScript()->registerCssFile($assetUrl.'/css/mainmenu.css');
?>