<?php
/* @var $this ProductController */
$assetsUrl = publish('application.assets');

$this->pageTitle=Yii::app()->name;
$this->metaTitle=Yii::app()->name;

$this->breadcrumbs=array(
	'客戶服務' /*=>array("/service",)*/,
	//'create',
);

?>
<?php 
	$this->widget('application.widgets.BlueHeader.BlueHeader', array('LabelTitle' => "客戶服務",));
?>


<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash_success">
	<div class="checked_div">
		<div class="checked_icon"></div>
		<div class="checked_label">
			<?php echo Yii::app()->user->getFlash('contact'); ?>
		</div>
		<div class="clear"></div>
	</div>
	<div class="button_div">
		<?php $this->widget('zii.widgets.jui.CJuiButton', array(
			'buttonType'=>'link',
			'name'=>'flash_success_btn',
			'caption'=>'回到博洋首頁',
			'url'=>array('/'),
		)); ?>
	</div>
	
</div>

<?php else: ?>
<div class="content_body">
	
	<!-- <p>If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.</p> -->
	<div class="explain">
		<p>注意事項：</p>
		<ul>
			<li>如果有任何對於商品上的相關問題或是買賣上的疑問，我們將會以最快的速度回復您的問題。</li>
			<li>請勿蓄意留下攻擊、謾罵、毀謗等意見留言。</li>
			<li><label class="marker">*</label> 星號部份為必填欄位。</li>
			<li>再此提醒您，如果你的需求或問題是很急迫的，建議您直接電洽(06)2540199，我們將會有專人替您服務。</li>
		</ul>
	</div>

	<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<?= $form->errorSummary($model, "請解決下列的錯誤"); ?>

		<div class="row">
			<?= $form->labelEx($model,'name'); ?>
			<?= $form->textField($model,'name',array('size'=>62,'maxlength'=>128)); ?>
			<?= $form->error($model,'name'); ?>
		</div>
	
		<div class="row">
			<?= $form->labelEx($model,'tel'); ?>
			<?= $form->textField($model,'tel' ,array('size'=>62,'maxlength'=>128)); ?>
			<?= $form->error($model,'tel'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($model,'email'); ?>
			<?= $form->textField($model,'email' ,array('size'=>62,'maxlength'=>128)); ?>
			<?= $form->error($model,'email'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($model,'subject'); ?>
			<?= $form->textField($model,'subject',array('size'=>62,'maxlength'=>128)); ?>
			<?= $form->error($model,'subject'); ?>
		</div>

		<div class="row">
			<?= $form->labelEx($model,'body'); ?>
			<?= $form->textArea($model,'body',array('rows'=>6, 'cols'=>60)); ?>
			<?= $form->error($model,'body'); ?>
		</div>

	<?php if(CCaptcha::checkRequirements()): ?>
		<div class="row">
			<?= $form->labelEx($model,'verifyCode'); ?>
			<div>
				<?php $this->widget('CCaptcha'); ?>
				<?php echo $form->textField($model,'verifyCode'); ?>
			</div>
			<div class="hint">Please enter the letters as they are shown in the image above.
				<br/>Letters are not case-sensitive.
			</div>
			<?= $form->error($model,'verifyCode'); ?>
		</div>
	<?php endif; ?>

		<div class="row buttons">
			<?= CHtml::submitButton('送出', array('class' => 'btn_blue_send', 'style'=>'width:100px;')); ?>
		</div>

	<?php $this->endWidget(); ?>

	</div><!-- form end-->

<?php endif; ?>
</div>

<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetsUrl.'/css/service/index.css');
$cs->registerCssFile($assetsUrl.'/css/button.css');
?>