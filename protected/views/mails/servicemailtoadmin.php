<?php
$baseUrl = Yii::app()->request->getBaseUrl(true);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<style>
    	a{color:#2078c3;}
		body{background-color:#ebecec;font-family:arial;color:#414141;}
		td{font-size:13px;color:#414141;}
		img{border:0px}
		label{font-weight:bold}

        table{width: 700px; font-size:13px;}
	</style>
	<title><?=$subject?></title>
</head>
<body>
<div style="width:1000px;height:auto;background-color:#ebecec;">
    <div style="width:1000px;height:50px;clear:both;"></div>
    <table align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#ebecec">
        <tr>
            <td id="header" bgcolor="#252525">
                <table cellspacing="0" border="0" height="64">
                    <tbody>
                        <tr> 
                            <!-- <td width="318" height="58">
                                <a href="<?//=$baseUrl?>">
                                        <img src="<?//=$baseUrl?>/images/epaper_logo.jpg" width="318" height="58" alt="NVESTO">
                                </a>
                            </td> -->
                            <td width="400" height="58">
                    	       <a id="header_name" style="color:#fff;text-decoration:none;font-size:30px;" href="<?=$baseUrl?>">博洋貿易｜BaoYang Inc</a>
                            </td>
                            <td width="362" align="right">
                    	       <div id="header_service" style="font-size:24px;color:#ffffff;line-height:32px;">客戶服務</div>
                    	       <div style="font-size:12px;color:#bababa;"></div>
                            </td>
                            <td width="20"></td>
                        <tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
    	   <td style="background-color:white;padding:20px;line-height:25px;">
                <center>
                    <div class="pop_framebg_border">
                        <div class="pop_framebg">
                            <div class="popResult_container" style="margin-top:25px;text-align:left;">
                                <h4>您好：從 <label><?=$name?></label>(<?=$email?>) 送出一筆新的服務請求。下列為相關資料內容。</h4>
                                <!-- <div style="text-align:left;width:350px;">
                                    服務務單編號： <br>
                                </div> -->
                                <div align="center">
                                    <table border="1" style="margin:5px; border-collapse:inherit; width:350px;">
                                        <tbody>
                                            <tr align="left" valign="top">
                                                <td>客戶姓名</td>
                                                <td><?=$name?></td>
                                            </tr>
                                            <tr align="left" valign="top">
                                                <td>客戶電話</td>
                                                <td><?=$tel?></td>
                                            </tr>
                                            <tr align="left" valign="top">
                                                <td>Eamil</td>
                                                <td><?=$email?></td>
                                            </tr>
                                            <tr align="left" valign="top">
                                                <td>詢問事項</td>
                                                <td><?=$subject?></td>
                                            </tr>
                                            <tr align="left" valign="top">
                                                <td>詳細內容</td>
                                                <td><?=$body?></td>
                                            </tr>
                                            <!-- <tr align="left" valign="top">
                                                <td>請求時間</td>
                                                <td></td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </center>
    	   </td>
        </tr>
        <tr>
            <td align="center" style="padding-top:5px;font-size:10px;color:#888888">
                <div style="width:700px;height:50px;clear:both;"></div>
            </td>
        </tr>
        </table>
    <div style="width:1000px;height:50px;clear:both;"></div>
</div>
</body>
</html>