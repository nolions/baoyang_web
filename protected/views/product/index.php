<?php
$baseUrl = Yii::app()->request->baseUrl;
$assetsUrl = publish('application.assets');

//$this->pageTitle=Yii::app()->name.' - 產品介紹';
$this->metaTitle=Yii::app()->name.' - 產品介紹';

$this->layout = 'product';

$this->breadcrumbs=array(
	'產品介紹'
);

?>
<?php $this->widget('application.widgets.BlueHeader.BlueHeader', array('LabelTitle' => "產品介紹",)); ?>
<div class="content_body">
	<div class="filter-container" style="position:relative;">
		<div class="left">
			<!-- <div class="filter"> -->
				<!-- 產業分類： -->
				<!-- <select id="industrySortSel" class="span2">
					<?php //foreach ($IndustrySorts as $IndustrySort): ?>
					<option value="<?//=$IndustrySort->id; ?>"><?//=$IndustrySort->name;?></option>
					<?php //endforeach; ?>				
				</select>-->
			<!-- </div> -->
			
			<?php 
				$typeOpts = CHtml::listData($ProductTypes, 'Type_id', function($course) {
					return CHtml::encode($course->ProductType->name);
				});
				$subTypeOpts = CHtml::listData($ProductSorts, 'id', 'name');
			?>
			<div class="filter" style="<?php echo (count($typeOpts)<=0 &&count($subTypeOpts)<=0)?"display:none;":""?>">
				產品選擇：
			<?php
				echo CHtml::dropDownList('productTypeSel', 'productTypeVal', $typeOpts, 
					array(
						'class'=>'span2',
						'style' => (count($typeOpts)<=0)?'display: none;':'display:inline-block;',
						'ajax' => array(
							'type' => 'GET',
							'url'=>CController::createUrl('product/AjaxGetTechnolog'),
							'update' => '#TechnologFilterDiv',
							'data' => array(
								"ProductType"=>'js:$("#productTypeSel").val()',
							),
						)

					)
				); 
				
				echo CHtml::dropDownList('ProductSortSel', 'ProductSortVal', $subTypeOpts, 
					array(
						'class' => 'span2',
						'style' => (count($subTypeOpts)<=0)?'display: none;':'display:inline-block;',
						'ajax' => array(
							'type' => 'GET',
							'url'=>CController::createUrl('product/AjaxGetTechnolog'),
							'update' => '#TechnologFilterDiv',
							'data' => array(
								"subProductType"=>'js:$("#ProductSortSel").val()',
							),
						)
					)
				);
			?>
			</div>
			<div id="TechnologFilterDiv" class="filter" style="displya:none;">
				<?php $this->renderPartial('_TechnologFilter', array(
					'TechnologIndexs' => $TechnologIndexs,
					'TechnologyIds' => $TechnologyIds,
				));?>
			</div>
			<div>
				<!-- <span><a href="#" id="SelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">全選</a></span> -->
				<span><a href="#" id="unSelAll_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">取消選取</a></span>
				<span><a href="#" id="uSel_Btn" class="btn_red_send left" style="width:100px; margin-right:5px;">反向選取</a></span>
				<div class="clear"></div>
			</div>
		</div>
		<div class="right" style="width:100px; position:absolute; bottom:10px; right:30px;">
			<!-- <a id="filterBtn" class="btn_blue_send">搜尋</a> -->
			<?=CHtml::ajaxLink('搜尋', array('product/AjaxGetProductItemsTable'), array(
					'data' => array(
						"subIndustryType"=>$SubIndustry,
						"subProductType"=>'js:$("#ProductSortSel").val()',
						"subTechnologys"=>'js:getTechnologysChecks()',
					),	
					'type' => 'GET',
					'update' => '#product_content',
					'beforeSend' => 'function(){
						$("#product_content").hide();
						$("#loading").show();
					}',
					'complete' => 'function(){
						$("#loading").hide();
						$("#product_content").show();
					}'
				),	
				array('id'=>'filterBtn', 'class' => 'btn_blue_send')
			);?>
		</div>
		<div class="clear"></div>
		
	</div>
	
	<center>
		<div id="loading" style="display:none;">
		</div>
		<div id="product_content">
			<?php $this->renderPartial('_ProductTable', array(
				'Products' => $Products,
			));?>
		</div>
	</center>
</div>

</div>

<?php
$cs = Yii::app()->getClientScript();
// Yii::app()->getClientScript()->registerScriptFile($AssetsUrl.'/js/product/product.js');
$cs->registerCssFile($assetsUrl.'/css/button.css');
$cs->registerCssFile($assetsUrl.'/css/product/index.css');
$cs->registerScriptFile($assetsUrl.'/js/product/product.js', CClientScript::POS_END);
$cs->registerScript('', "
	$('#productTypeSel').change(function(){	
		getProductSort('$bastUrl', $('select[name=productTypeSel]').val());
	});

	$('#SelAll_Btn').click(function(){
		selAll();
		return false;
	});

	$('#unSelAll_Btn').click(function(){
		unselAll();
		return false;
	});

	$('#uSel_Btn').click(function(){
		usel();
		return false;
	});
", CClientScript::POS_END);
?>