<?php 
	$test = array(
		'物化數據' => array('ph' => '6-7', '比重' => '1.5', '吸油量'=>'45'), 
		'技術指標' => array('耐光'=>'4', '耐水'=>'4', '耐酸'=>'5', '耐鹼'=>'5')
	);
	$test = CJSON::encode($test);	
?>
<table class="ProductTable">
	<thead>
		<tr>
			<th>產品品名</th>
			<th>國際索引碼</th>
			<th>本色</th>
			<th>沖淡色</th>
			<th>類型</th>
			<th>對照產品</th>
			<th>外觀顏色</th>
			<th></th>
		</tr>
	</thead>
	<tbody class="ProductsList">
	<?php if(count($Products)>0){?>
		<?php foreach($Products as $Product => $value){?>
		<tr>
			<td class="ProductName"><?=$value['Product_name']?></td>
			<td class="ProductInde"><?=$value['Product_index']?></td>
			<td class="ProductColor_1 color"><div style="background-color:<?=$value['Color_1']?>;"></div></td>
			<td class="ProductColor_2 color"><div style="background-color:<?=$value['Color_2']?>;"></div></td>
			<td><?=$value['Product_type']?></td>
			<td><?=$value['ContrastProduct']?></td>
			<td><?=$value['Exterior']?></td>
			<td width='20'><a href="#"><div class="arrow <!--arrow_down-->">更多</div></a></td>
		</tr>
		<tr style="display:none;">
			<td colspan="8">
				<table style="margin:0;">
					<?php if ($value['Technology'] != null && strlen($value['Technology']) !=0): ?>
					<tr>
						<td style="border:0;">
						<?php $this->widget('application.widgets.ProductTargetTable.ProductTargetTable', array(
							'data' => $value['Technology'],
							'title' => '技術指標',
						));?>
						</td>
					</tr>
					<?php endif ?>
					<?php if ($value['Physical'] != null && strlen($value['Physical']) !=0): ?>
					<tr>
						<td style="border:0;">
						<?php $this->widget('application.widgets.ProductTargetTable.ProductTargetTable', array(
							'data' => $value['Physical'],
							'title' => '物性數據',
						));?>
						</td>
					</tr>
					<?php endif ?>
					<?php if ($value['Material'] != null && strlen($value['Material']) !=0): ?>
					<tr>
						<td style="border:0;">
						<?php $this->widget('application.widgets.ProductTargetTable.ProductTargetTable', array(
							'data' => $value['Material'],
							'title' => ' 成份資料',
						));?>
						</td>
					</tr>
					<?php endif ?>
					<?php if ($value['Other'] != null && strlen($value['Other']) !=0): ?>
					<tr>
						<td style="border:0;">
						<?php $this->widget('application.widgets.ProductTargetTable.ProductTargetTable', array(
							'data' => $value['Other'],
							'title' => '其他資訊',
						));?>
						</td>
					</tr>
					<?php endif ?>
				</table>
			</td>
		</tr>
		<?php } ?>
	<?php }else{ ?>
		<tr><td colspan='8' style="text-align:center">沒有符合條件的產品</td></tr>
	<?php } ?>
	</tbody>
</table>
<script type="text/javascript">
	$('.arrow').click(function(){	
		if($(this).parent().parent().parent().next().is(':visible') )
			$(this).empty().append("更多");
			//$(this).removeClass('arrow_left').addClass('arrow_down');
		else
			$(this).empty().append("隱藏");
			//$(this).removeClass('arrow_down').addClass('arrow_left');

		$(this).parent().parent().parent().next().toggle();

		return false;
	});
</script>
<?php
$assetsUrl = publish('application.assets');
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($assetsUrl.'/css/product/_table.css');
?>