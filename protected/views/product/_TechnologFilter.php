<!--<table>
	<?php //foreach ($TechnologIndexs as $TechnologIndex => $value) : ?>
	<tr>
		<td style="width:70px;"><?php //echo $value['Technology']['name']."："; ?></td>
			<td style="">
			<?php /*$Data = array(0=>'ALL') + CHtml::listData(SubTechnology::model()->findAll(
				'Technology_id ="'.$value['Technology_id'].'"'), 
				'id', 
				'name');
				
			echo CHtml::radioButtonList('Technolog_'.$value['Technology_id'], '0',
				$Data,
				array(
					'labelOptions'=>array('style'=>'display:inline'), // add this code
					'separator'=>'',
					'class' => '1',
					'template'=>'<span style="margin:0 3px;">{input}{label}</span>'
				)
			);*/?>
			</td>					
	</tr>
	<?php //endforeach; ?>
</table>-->
<div style="color:red;">
	*提醒您有效選擇下列相關篩選數值，將可以協助你找到想要的產品。<br>
	*如果不確定所需要的篩選數值可以所有項目皆不要勾選，系統將會為您顯示該產業及類型的所有產品。
</div>
<div></div>
<table>
	<?php foreach ($TechnologIndexs as $TechnologIndex => $value) : ?>
	<tr>
		<td style="width:70px;"><?php echo $value['Technology']['name']."："; ?></td>
			<td style="">
			
			<?php echo CHtml::checkBoxList(
				'Technology', $TechnologyIds,
				CHtml::listData(SubTechnology::model()->findAll(
					'Technology_id ="'.$value['Technology_id'].'"'), 
					'id', 
					'name'),
				array(
					'labelOptions' => array(
						'style' => "display: inline-block; margin-right: 10px;",
					),
			        'separator'=>'',
			        'template'=>'{input}&nbsp;{label}',
			        'class' => 'technology_item',
			        'baseID' => $value['Technology']['id'],
				)); ?>
			</td>					
	</tr>
	<?php endforeach; ?>
</table>