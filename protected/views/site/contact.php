<?php
$baseUrl = publish('application.assets');

$this->pageTitle=Yii::app()->name;
$this->metaTitle=Yii::app()->name;

$this->breadcrumbs=array(
	'聯絡方式',
);

?>

<?php 
	$this->widget('application.widgets.BlueHeader.BlueHeader', array('LabelTitle' => "聯絡方式",));
?>
<div class="content_body">	
	<div class="cb">
		<div class="content_lab">電話：</div>
		<div class="content_val"><?=$info['tel']?></div>
		<div class="clear"></div>
	</div>
	<div class="cb">
		<div class="content_lab">傳真：</div>
		<div class="content_val"><?=$info['fax']?></div>
		<div class="clear"></div>
	</div>
	<div class="cb">
		<div class="content_lab">Email： </div>
		<div class="content_val"><?=$info['email']?></div>
		<div class="clear"></div>
	</div>
	<div class="cb">
		<div class="content_lab">網址： </div>
		<div class="content_val"><?=Yii::app()->createAbsoluteUrl('', array(), 'http');?></div>
		<div class="clear"></div>
	</div>
	<div class="cb">
		<div class="content_lab">地址： </div>
		<div class="content_val"><?php echo $info['zip']. $info['address']?></div>
		<div class="clear"></div>
	</div>
</div>
<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl.'/css/site/contact.css');
?>