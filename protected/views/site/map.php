<?php
$baseUrl = publish('application.assets');

$this->pageTitle=Yii::app()->name;
$this->metaTitle=Yii::app()->name;

$this->breadcrumbs=array(
	'服務據點',
);

$width = 700;
$height = 300;
?>

<?php 
	$this->widget('application.widgets.BlueHeader.BlueHeader', array('LabelTitle' => "服務據點",));
?>

<div class="content_body">
<?php
	$this->widget('application.widgets.GoogleMaps.GoogleMaps', array(
		'width' => $width,
		'height' => $height,
		'address' => $info['address'],
		'zip' => $info['zip'],
	));
?>	
</div>
<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl.'/css/site/map.css');
?>