<?php
/* @var $this SiteController */
$baseUrl = publish('application.assets');

$this->layout='main';
$this->pageTitle=Yii::app()->name;
$this->metaTitle=Yii::app()->name;
?>

<?php 
	$this->widget('application.widgets.BlueHeader.BlueHeader', array('LabelTitle' => "關於博洋",));
?>
<div class="content_body">
	<div id="logo">
		<?=CHtml::image($baseUrl. '/images/site/logo_m.png', 'logo'); ?>
	</div>
	<?=$info['content']?>
	<!-- <div id="about_describe_1" class="about_describe">
		<h3 class=""> 博洋簡介</h3>
		<p id="content-Introduction" class="content-label">
			<label id="Str_first_1" class="Str_first" style="color:#ff4500;">博</label>
			洋貿易成立於2005年，是一家專業的化工原料供應商，位於台灣省台南市，致力於開發橡塑膠、各類塗料、油墨、建築材料、化妝品、色漿色膏等相關產業，舉凡各類有機顏料、無機顏料、染料、鈦白粉、碳黑、珠光顏料、複合環保顏料、螢光顏料、鋁銀粉鋁銀漿、銅粉、各類樹脂、添加劑等等。並且與大陸及國內外知名大廠合作，成為台灣主要的經銷商。
		</p>
	</div>
	
	<div id="about_describe_2" class="about_describe">
		<h3 class="rounded-top-corners">經營理念</h3>	
		<p id="content-Idea" class="content-label" style="padding:0;">
			<label id="Str_first_2" class="Str_first" style="color:#0000ff;">博</label>洋貿易公司的四大經營理念分別為：『專業』、『務實』、『突破』、『服務』。
		</p>
	</div>

	<div id="about_describe_3" class="about_describe">
		<h3 class="rounded-top-corners">未來展望</h3>
		<p id="content-Futurework" class="content-label">
			<label id="Str_first_3" class="Str_first" style="color:#ffa500;">博</label>洋公司將持續貫徹經營理念，以專業為出發點、務實為基礎突破為導向，服務為動力。以邁向一流的專業化工貿易商為目標，與客戶攜手共創雙贏的未來。
		</p>
	</div> -->
	
</div>
<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl.'/css/site/index.css');
?>




