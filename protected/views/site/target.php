<?php
$baseUrl = publish('application.assets');

$this->pageTitle=Yii::app()->name;
$this->metaTitle=Yii::app()->name;

$this->breadcrumbs=array(
	 '博洋願景',
);

?>

<?php 
	$this->widget('application.widgets.BlueHeader.BlueHeader', array('LabelTitle' => "博洋願景",));
?>
<div class="content_body">
	<div class="colorgradient">
		『專業』、『務實』、『突破』、『服務』
	</div>
	
	<?=$info['content']; ?>
	<!-- <ul class="target_ul">
		<li>
			<label id="Str_first_1" class="Str_first">專業</label> - 業務人員在進入市場與客戶接洽前均受過現場實際操作訓練，可配合客戶在實際生產作業時遭遇的問題提出適切的解決方案及建議適合的使用產品，以專業角度來切入，迅速提供解決方案。
		</li>
		<li>
			<label id="Str_first_2" class="Str_first">務實</label> - 對於出貨的產品我們秉持務實的原則，所有的原料產品均經過嚴格的內部檢測及監控後才出貨，確保客戶在產品的使用上可以安心放心。
		</li>
		<li>
			<label id="Str_first_3" class="Str_first">突破</label> - 首創原料篩選系統，提昇服務效率與品質。客戶可以預先利用我司網站篩選系統提出原料需求，加快解決問題速度。不以現有產品而滿足，持續搜尋優質廠商及產品，提供給客戶最新最快的產品資訊。
		</li>
		<li>
			<label id="Str_first_4" class="Str_first">服務</label> - 售後服務是博洋公司極為重視的環節，對於我們來說，產品只是一個附加價值，我們銷售的是服務，我們在意客戶提出的每一個意見，持續反省本身的做法，提供更好的服務品質給客戶。
		</li>
	</ul> -->
</div>
<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl.'/css/site/target.css');
?>