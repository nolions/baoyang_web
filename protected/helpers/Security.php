<?php
class Security{
	/**
	 * The encrypt method
	 * {@see http://php.net/manual/en/mcrypt.ciphers.php}
	 */
	public static $cipher = MCRYPT_DES;

	/**
	 * The key to encrypt/decrypt cipher
	 */
	public static $key = 'mqgt7tnw';

	/**
	 *	This two functions are php version of python's URL-safe base64 encoder / decoder.
	 *
	 *	When embedding binary data in URL, it is preferable to use base64 encoding. However, two
	 *	characters ('+' and '/') used in the standard base64 encoding have special meanings in URLs,
	 *	often leading to re-encoding with URL-encoding, or worse, interoperability problems.
	 *
	 *	To overcome the problem, these two functions provides a variation of base64 codec compatible with
	 *	python's urlsafe_b64encode / urlsafe_b64decode.
	 *
	 * @param string $string the string to be encoded
	 * @return string
	 */
	public static function urlsafe_b64encode($string) {
		$data = base64_encode($string);
		$data = str_replace(array('+','/','='),array('-','_',''),$data);
		return $data;
	}

	/**
	 * {@see Security::urlsafe_b64encode}
	 *
	 * @param string $string the string to be decoded
	 * @return string
	 */
	public static function urlsafe_b64decode($string) {
		$data = str_replace(array('-','_'),array('+','/'),$string);
		$mod4 = strlen($data) % 4;
		if ($mod4) {
			$data .= substr('====', $mod4);
		}
		return base64_decode($data);
	}

	/**
	 * Decode a object by json_encode->base64_encode
	 *
	 * @param mixed
	 * @return string cipher
	 */
	public static function j64encode($o) {
		return base64_encode(json_encode($o));
	}

	/**
	 * Decode a cipher by base64_decode->json_decode
	 *
	 * @param string $cipher
	 * @return mixed
	 */
	public static function j64decode($cipher) {
		$r = base64_decode($cipher);
		$r = json_decode($r, true);

		return $r;
	}

	/**
	 * Encrypt data
	 *
	 * @param string $msg
	 * @return string
	 */
	public static function encrypt($msg) {
		$secret = mcrypt_encrypt(self::$cipher, self::$key, $msg, 'ecb');
		$urlSafeSecret = self::urlsafe_b64encode($secret);

		return $urlSafeSecret;
	}

	/**
	 * Decrypts data
	 *
	 * @param string $cipher
	 * @return string
	 */
	public static function decrypt($urlSafeSecret) {
		$secret = self::urlsafe_b64decode($urlSafeSecret);
		$msg = mcrypt_decrypt(self::$cipher, self::$key, $secret, 'ecb');

		return rtrim($msg,"\0");
	}

	public static $secretTokenSalt = "ZKlDrvjH0L6p2k";
	public static function secretToken($i) {
		return hash("md5",$i.self::$secretTokenSalt);
	}
}
