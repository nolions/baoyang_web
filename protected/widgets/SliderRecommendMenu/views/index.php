<div class="SubMenu">
    <div class="SubMenu_Header">
        <label class="plus">+</label><label class="SildeMenu_Label"><?=$title ?></label>
    </div>
    <ul class="nav">
    
    <?php foreach ($items as $item => $value){ ?>
        <li class="SubMenu_Item">
        	<div><?=CHtml::link($value['name'], array($value['url']))?></div>
        </li>
    <?php } ?>
    </ul>
</div>

<?php
$widgetAssetsUrl = publish('application.widgets.SliderRecommendMenu.assets');
Yii::app()->getClientScript()->registerCssFile($widgetAssetsUrl.'/css/style.css');
?>