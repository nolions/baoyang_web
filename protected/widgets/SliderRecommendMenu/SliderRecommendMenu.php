<?php
/**
 * show the stock infomation
 */
class SliderRecommendMenu extends CWidget {

    public $title;
	public $items;
 
    public function run() {
    	
    	$this->render('index', array(
            'title' =>$this->title,
			'items' => $this->items,
		));
    }
}