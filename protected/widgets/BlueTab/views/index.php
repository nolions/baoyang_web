<div class="bluetab-widget">
	<ul class="blue-tabs">
		<?php
		$i = 0;
		foreach ($items as $k=>$v) {
			if ($i==$default_on) {
				echo '<li class="tab_on">',$k,'</li>';
			}
			else {
				echo '<li class="tab_off"><a href="',$v,'">',$k,'</a></li>';
			}
			$i++;
		}
		?>
	</ul>
</div>

<?php
	$widgetAssetsUrl = publish('application.widgets.BlueTab.assets');
	Yii::app()->getClientScript()->registerCssFile($widgetAssetsUrl.'/css/bluetab.css');
?>
