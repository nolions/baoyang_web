<?php
/**
 * show the stock infomation
 */
class BlueTab extends CWidget {
	public $items;
	public $on;
	
	public function run() {
		
		// $current_controller = Yii::app()->controller->id;
		$current_action = Yii::app()->controller->action->id;
		$i = 0;
		foreach ($this->items as $k=>$v) {
			if (strstr($v,$current_action)) {
				$this->on = $i;
			}
			$i++;
		}
		
		$this->render('index', array(
			'items' => $this->items,
			'default_on' => $this->on,
			'ca' => $current_action,
		));
	}
}
