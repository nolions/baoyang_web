<div class="IndustrySliderMenu">
    <div class="IndustrySliderMenu_Header">
        <label class="SildeMenu_Label">產業分類</label>
    </div>
    <table class="IndustryTable">
    <?php foreach ($Industrys as $Industry => $IndustryValue): ?>
         <tr class="IndustryTr">
            <td width="20" class="IndustryTableTd"></td>
            <td width="70" class="IndustryTableTd"><?=$IndustryValue['name']?></td>
            <td width="20" class="IndustryTableTd"><div class="toggle icon_sideemenu_more"></div></td>
        </tr>
        <tr class="SubIndustryTr" style="display:none;">
            <td colspan="2">
                <table id='aa' class="SubIndustryTable">
                <?php
                    $SubIndustrys = SubIndustry::model()->findAll(
                        'Industry_id=:Industry_id', 
                        array(':Industry_id'=>$IndustryValue['id'])
                    );

                    foreach ($SubIndustrys as $SubIndustry => $SubIndustryValue):
                ?>
                    <tr id="<?=$SubIndustryValue['id']?>">
                         <?php if($_GET['sort'] == $SubIndustryValue["id"]):
                                echo "<script>";
                                echo "var TagVal = $('#{$SubIndustryValue["id"]}').parent().parent().parent().parent();";
                                echo 'TagVal.show();';
                                echo 'TagVal.prev().addClass("sidemenuon");';
                                echo 'TagVal.prev().find(".toggle").addClass("icon_sideemenu_less").removeClass("icon_sideemenu_more");';
                                echo "</script>";
                        endif;?>
                        <td width="16">
                            <div class="<?= ($_GET['sort'] == $SubIndustryValue['id'])?"SubIndustry_active":"" ?>"></div>
                        </td>
                        <td>
                            <a href="<?=Yii::app()->createUrl('/product/index' ,array('sort'=>$SubIndustryValue['id']))?>">
                                <?=$SubIndustryValue['name']?>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </table>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>

<?php
$widgetAssetsUrl = publish('application.widgets.SliderIndustryMenu.assets');
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($widgetAssetsUrl.'/css/style.css');
$cs->registerScriptFile($widgetAssetsUrl."/js/index.js", CClientScript::POS_END);
$cs->registerScript('search', "
        
    ", CClientScript::POS_END 
);
?>