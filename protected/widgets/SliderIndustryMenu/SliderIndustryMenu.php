<?php
/**
 * 
 * show the stock infomation
 */
class SliderIndustryMenu extends CWidget {

	public function run() {

        $Industrys = Industry::model()->findAll();

		$this->render('index', array(
			// 'menu' => $this->getIndustry(),
            'Industrys' => $Industrys,
		));
	}

    private function getIndustry()
    {
        $IndustryTypes = Industry::model()->findAll();
        
        foreach($IndustryTypes as $IndustryType => $value) {
            $IndustrySorts = SubIndustry::model()->findAll("`Industry_id` =".$value['id']);

            $IndustrySortArray=null;
            foreach ($IndustrySorts as $IndustrySort => $IndustrySortValue)
                $IndustrySortArray[$IndustrySortValue['id']] = $IndustrySortValue['name'];
            
            $menus[$value['id']] = array($value['name'] => $IndustrySortArray);
        }

        return $menus;
    }
}