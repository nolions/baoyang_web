$('tr[class=IndustryTr]').click(function(){
        if($(this).next('.SubIndustryTr').is(':visible')){
            $(this).find('.toggle').addClass('icon_sideemenu_more').removeClass('icon_sideemenu_less');
        	$(this).removeClass("sidemenuon");
        }else{
            $(this).find('.toggle').addClass('icon_sideemenu_less').removeClass('icon_sideemenu_more');
            $(this).addClass("sidemenuon");
        }

        $(this).next('.SubIndustryTr').toggle();
    });