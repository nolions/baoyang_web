<?php
/**
 * Render the QR code
 *
 * @see https://developers.google.com/chart/infographics/docs/qr_codes
 */
class QrcodeWidget extends CWidget {
    public $width = 150;
    public $height = 150;
    public $size = 'L';
    public $data;
    public $border = 1;
    public $alt = 'QR Code';

    public function run() {
        if (empty($this->data)) {
            throw new Exception('Try to generate QR code with empty data');
        }

        $src = sprintf(
            'https://chart.googleapis.com/chart?cht=qr&chs=%dx%d&chl=%s&chld=%s|%d',
            $this->width, //width
            $this->height, //height
            urlencode($this->data), //string data,
            $this->size,
            $this->border
            //$this->border
        );

        echo CHtml::image($src, $this->alt, array(
            'style' => sprintf('width:%s; height:%s', $this->width, $this->height)
        ));
    }
}