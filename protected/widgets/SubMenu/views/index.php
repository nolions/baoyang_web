<div class="SubMenu">

<?php foreach ($items as $key => $value) { ?>
    <div class="SubMenu_Header">
        <label class="SildeMenu_Label"><?=$key ?></label>
    </div>
    <ul class="nav">

    <?php foreach ($value as $item => $itmeValue) { 
        if($this->controller->id == 'product'):?>
        <li class="SubMenu_Item <?=($itmeValue['on']==$id)?'SubMenu_active':''?>">
    <?php else: ?>
        <li class="SubMenu_Item <?=($itmeValue['url']==$url)?'SubMenu_active':''?>">
    <?php endif; ?>            
        	<div><?=CHtml::link($item, array($itmeValue['url']))?></div>
        </li>
    <?php } ?>
    </ul>
<?php } ?>
</div>

<?php
$widgetAssetsUrl = publish('application.widgets.SubMenu.assets');
Yii::app()->getClientScript()->registerCssFile($widgetAssetsUrl.'/css/style.css');
?>