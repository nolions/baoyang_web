<?php
/**
 * show the stock infomation
 */
class SubMenu extends CWidget {

	public $items;
 
    public function run() {
    	$url = '/'.Yii::app()->controller->id.'/'.Yii::app()->controller->action->id;
        
    	$id=1;
        if (isset($_GET['id']))
            $id=$_GET['id'];
    	
    	$this->render('index', array(
			'items' => $this->items,
			'url' => $url,
			'id'=> $id,
		));
    }
}