<?php
/**
 * show the stock infomation
 */
class BlueHeader extends CWidget {
 
	public $LabelTitle = "";

    public function run() {

        $this->render('index', array(
        	'title' => $this->LabelTitle
		));
    }
}