<div class="content_header">
    <div class="content_title"><?=$title?></div>
    <div id="title_line_1" class="content_title_line"></div>
    <div id="title_line_2" class="content_title_line"></div>
    <div id="title_line_3" class="content_title_line"></div>
    <div class="clear"></div>
</div>
<?php
$widgetAssetsUrl = publish('application.widgets.BlueHeader.assets');
Yii::app()->getClientScript()->registerCssFile($widgetAssetsUrl.'/css/style.css');
?>