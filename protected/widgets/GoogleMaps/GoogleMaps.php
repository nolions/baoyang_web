<?php
/**
 * Render the Map throught google maps
 *
 * @see https://developers.google.com/chart/infographics/docs/qr_codes
 */
class GoogleMaps extends CWidget {
	//google set information
	public $width = 700;
    public $height = 300;
    public $zoom = 17;

    //address information
    public $zip = '710';
    public $address = '台南市永康區新中街331號';
    
	public function run() {
		$fullAddress = $this->zip. ' '. $this->address;
		
		$this->render('index', array(
			'width' => $this->width,
			'height' => $this->height,
			'zoom' => $this->zoom,
			'address' => $fullAddress
		));
	}
}
