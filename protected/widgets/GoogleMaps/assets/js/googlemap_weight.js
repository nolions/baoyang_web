$.fn.googlemap = function(address, zoom, width, height)
{
	var src = "";
    
    $(this).each(function(){
    var z = $(this);
    
    src = "https://maps.google.com/maps?"
        +"&q=" + address
        +"&hnear=" + address
        +"&z=" +  zoom
        +"&output=embed";
        z.html("<iframe src = '" + src + "' width = '" + width + "' height = '"+ height + "'></iframe>");
    });

    return src;
}