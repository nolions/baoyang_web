<div id='map'>
</div>
<script>$('#map').googlemap("<?=$address?>", <?=$zoom?>, <?=$width?>, <?=$height?>);</script>

<?php
$widgetAssetsUrl = publish('application.widgets.GoogleMaps.assets');
Yii::app()->clientScript->registerCoreScript('jquery'); 
Yii::app()->getClientScript()->registerScriptFile($widgetAssetsUrl.'/js/googlemap_weight.js');
?>
