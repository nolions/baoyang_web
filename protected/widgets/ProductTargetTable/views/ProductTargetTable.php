<table class="ProductTable SubTable">
	<tr>
	<?php
		$DataArray = CJSON::decode($data, true);
	?>
		<td colspan="<?=count($DataArray)?>"><?=$title?></td>	
	</tr>
	<tr>
	<?php foreach ($DataArray as $key => $value) {	?>
		<td><?=$key ?></td>
	<?php } ?>
	</tr>
	<tr>
	<?php foreach ($DataArray as $key => $value) {	?>
		<td style="text-align:left;"><?=$value ?></td>
	<?php } ?>
	</tr>
</table>
<?php
$widgetAssetsUrl = publish('application.widgets.ProductTargetTable.assets');
Yii::app()->getClientScript()->registerCssFile($widgetAssetsUrl.'/css/ProductTargetTable.css');
?>