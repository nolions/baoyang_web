<?php
/**
 * 
 */
class ProductTargetTable extends CWidget {
	public $data;
	public $title='';

    public function run() {

        $this->render('ProductTargetTable', array(
        	'title' => $this->title,
        	'data' => $this->data,
		));
    }
}