<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'timeZone' => 'Asia/Taipei',
	'language'=>'zh_tw',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'博洋貿易',

	// preloading 'log' component
	'preload'=>array(
		'log',
		'bootstrap'
	),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.sort.*',
		'application.models.admin.*',
		'application.models.web.*',
		'application.models.forms.*',
		'application.components.*',
		'application.helpers.*',
	),

	'modules'=>array(
		'admin' => array(
			'controllerMap' => array(
				'default' => 'application.modules.admin.controllers.DefaultController',
			),
			'components'=>array(
				'urlManager'=>array(
					'showScriptName'=>false,
					'urlFormat'=>'path',
					'urlSuffix'=>'',
					'caseSensitive' => true
				),
				'bootstrap' => array(
					'class' => 'application.extensions.yii-bootstrap.components.Bootstrap',
				),
			)
		),
		
		// uncomment the following to enable the Gii tool
		/*
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'Enter Your Password Here',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		*/
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl' => array('/admin'),
		),

		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
            'caseSensitive'=>false,
			'rules'=>array(
				'/'=>'site/index',
				'product/<sort\d+>'=>'product/index',
				'product/<_controller>'=>'product/<_controller>',
				// 'product/ajaxGetProductSort'=>'product/ajaxGetProductSort',
				// 'product/<sort>'=>'product/index',

				// '<controller>'=>'<controller>/index',
				'<controller>/<action>'=>'<controller>/<action>',

				//'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				//'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				//'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		'bootstrap' => array(
			'class' => 'application.extensions.bootstrap.components.Bootstrap'
		),

		'mail'=>array(
			'class' => 'application.extensions.yii-mail.YiiMail',
			'transportType'=>'smtp',
                'transportOptions'=>array(
                        // 'host'=>'smtpout.asia.secureserver.net',
                        // 'encryption'=>'ssl',
                        // 'username'=>'kevin.chen@smims.com',
                        // 'password'=>'dx110628',
                        // 'port'=>'465',
                        'host'=>'sp22.g-dns.com',
                        'encryption'=>'ssl',
                        'username'=>'admin@baoyang-tw.com',
                        'password'=>'admin1234',
                        'port'=>'465',                      
                ),
			'viewPath' => 'application.views.mails',
		),

		// database settings are configured in database.php
		'dbSort'=>array(
			'class' => 'system.db.CDbConnection',
			'connectionString'=>'mysql:host=localhost;dbname=baoyang_sort',
			'username'=>'root',
			'password'=>'1234',
			'charset' => 'utf8',
			'emulatePrepare'=>true,
			// 'class' => 'system.db.CDbConnection',
			// 'connectionString'=>'mysql:host=localhost;dbname=baoyangt_baoyang_sort',
			// 'username'=>'baoyangt_admin',
			// 'password'=>'baoyang12345',
			// 'charset' => 'utf8',
			// 'emulatePrepare'=>true,
		),

		'dbAdmin'=>array(
			'class' => 'system.db.CDbConnection',
			'connectionString'=>'mysql:host=localhost;dbname=baoyang_admin',
			'username'=>'root',
			'password'=>'1234',
			'charset' => 'utf8',
			'emulatePrepare'=>true,
			// 'class' => 'system.db.CDbConnection',
			// 'connectionString'=>'mysql:host=localhost;dbname=baoyangt_baoyang_admin',
			// 'username'=>'baoyangt_admin',
			// 'password'=>'baoyang12345',
			// 'charset' => 'utf8',
			// 'emulatePrepare'=>true,
		),

		'dbWeb'=>array(
			'class' => 'system.db.CDbConnection',
			'connectionString'=>'mysql:host=localhost;dbname=baoyang_Web',
			'username'=>'root',
			'password'=>'1234',
			'charset' => 'utf8',
			'emulatePrepare'=>true,
			// 'class' => 'system.db.CDbConnection',
			// 'connectionString'=>'mysql:host=localhost;dbname=baoyangt_baoyang_Web',
			// 'username'=>'baoyangt_admin',
			// 'password'=>'baoyang12345',
			// 'charset' => 'utf8',
			// 'emulatePrepare'=>true,
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'projectName'=>'baoyang for web',
		'version' => '1.0.0.0',
		'noreplyEmail'=>'noreply@baoyang.com',
		'adminEmail'=>'lv2410@gmail.com',
		'serviceEmail'=>'lv2410@gmail.com',
		'pmEmail'=>'lv2410@gmail.com',
	),
);
