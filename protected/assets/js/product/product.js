function getProductSort(url, type)
{
	var Select_ElementTag = $('select[id=ProductSortSel]');
	var SearchBtn_ElementTag = $('a[id=filterBtn]');
	SearchBtn_ElementTag.hide();

	$.ajax({
		url: url,
		data: {"ProductType": type},
		type:"GET",
		dataType:'json',
		success: function(msg){
			// alert(msg.length);

			Select_ElementTag.empty()
			if (msg.length > 0) {
				// Select_ElementTag.append('<option value="0">所有類別</option>');
				for(var key in msg){ 
					Select_ElementTag.append('<option value="' + msg[key]['id'] + '">'+msg[key]['name']+'</option>');	
				}
				Select_ElementTag.show();
			}else{
				Select_ElementTag.hide();
			}

			SearchBtn_ElementTag.show();
		},
		error:function(xhr, ajaxOptions, thrownError){ 
			//alert(xhr.status);
			//alert(thrownError);
			SearchBtn_ElementTag.show();
		}
	});
}

function getTechnologysChecks(){
	var favorite = [];
	$.each($('.technology_item:checked'), function(){ 
		favorite.push($(this).val());
	});
	//alert('My favourite sports are: ' + favorite.join(','));

	return favorite.join(',');
}

function selAll(){
	$('.technology_item').each(function(){
		$('.technology_item').prop('checked',true);
	});
}

function unselAll(){
	$('.technology_item').each(function(){
		$('.technology_item').prop('checked',false);
	});
}

function usel(){
	jQuery.each($('.technology_item'), function(i, n){
   		n.checked = !n.checked;
   	});
}

// $('.arrow').click(function(){	
// 	if($(this).parent().parent().parent().next().is(':visible') )
// 		$(this).removeClass('arrow_left').addClass('arrow_down');
// 	else
// 		$(this).removeClass('arrow_down').addClass('arrow_left');

// 	$(this).parent().parent().parent().next().toggle();

// 	return false;
// });