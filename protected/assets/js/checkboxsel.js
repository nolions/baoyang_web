function selAll(IdTag){
	var element = $(IdTag);
	$(element).each(function(){
		$(element).prop('checked',true);
	});
}

function unselAll(IdTag){
	var element = $(IdTag);
	$(element).each(function(){
		$(element).prop('checked',false);
	});
}

function usel(IdTag){
	var element = $(IdTag);
	jQuery.each($(element), function(i, n){
   		n.checked = !n.checked;
   	});
}