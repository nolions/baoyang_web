<?php

class SiteController extends Controller
{
	public $submenuOn = 1;

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		//set sub menu items
		$this->summenu = $this->setSubmenu();
		$this->submenuOn = 1;
		
		$info = Info::model()->getSiteAbout();

		$this->render('index', array(
			'info' => CJSON::decode($info, true)
		));
	}

	public function actionMap()
	{
		//set sub menu items
		$this->summenu = $this->setSubmenu();
		$this->submenuOn = 3;

		$info = Info::model()->getSiteInfo();

		$this->render('map', array(
			'info' => CJSON::decode($info, true)
		));
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		//set sub menu items
		$this->summenu = $this->setSubmenu();
		$this->submenuOn = 4;

		$info = Info::model()->getSiteInfo();
		
		$this->render('contact', array(
			'info' => CJSON::decode($info, true)
		));
	}

	public function actionTarget()
	{
		//set sub menu items
		$this->summenu = $this->setSubmenu();
		$this->submenuOn = 2;

		$info = Info::model()->getSiteTarget();

		$this->render('target', array(
			'info' => CJSON::decode($info, true)
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	function setSubmenu()
	{
		$summenu = array( 
			'關於我們' =>array(
			'關於博洋' => array('url'=>'/site/index'),
			'博洋願景' => array('url'=>'/site/target'),
			'服務據點' => array('url'=>'/site/map'),
			'聯絡方式' => array('url'=>'/site/contact'),
		));

		return $summenu;
	}
}