<?php

class ServiceController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	public function actionIndex()
	{
		$model = new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name= $model->name;
				$tel=$model->tel;
				$email=$model->email;
				$subject=$model->subject;
				$body=$model->body;
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";
				
				//mail(Yii::app()->params['adminEmail'],$subject,$model->body, $headers);
				
				
				// $this->SendMail($name, $email, $tel, $subject, $body);
				
				//Send Mail to Guest's email
				if ($email != null) {
					$this->SendMail($email, $name, $email, $tel, $subject, $body, 'guest');
				}

				//Send Mail to Admin's email
				$info = CJSON::decode(Info::model()->getSiteInfo(), true);
				if($info['email'] != null){
					$this->SendMail($info['email'], $name, $email, $tel, $subject, $body, 'service');
				}
				
				
				//Store to Database
				$this->StoreUserSuggest($name, $email, $tel, $subject, $body);
 		
				Yii::app()->user->setFlash(
					'contact',
					'您的意見已經算寄出。在收到您的寶貴意見後，我們將會在第一時間派人專責處理，並儘速與您聯絡。<br>博洋貿易在此感謝您，您們寶貴意見將會是我們持續進度動力。'
				);
				
				$this->refresh();
			}
		}
		$this->render('index', 
			array(
				'model'=>$model
		));
	}

	function SendMail($sendTo, $GuestName, $GuestEmail, $tel, $subject, $body, $model='guest')
	{
		Yii::import('application.extensions.yii-mail.YiiMailMessage');

		$controller = Yii::app()->controller;

		$output = $controller->renderInternal(dirname(__FILE__).'/../views/mails/serviceTemplate.php',array(
			'name'=> $GuestName,
			'email'=> $GuestEmail,
			'tel' => $tel,
			'subject'=> $subject,
			'body'=>$body,
			'model'=>$model
			//'Td'=> Security::encrypt($bill->id),
			//'bill'=>$bill,
		), true);
		

		$message = new YiiMailMessage;
		$message->subject = '博洋貿易客戶服務通知信件';
		$message->setBody(
                    $output,
                    'text/html'
            );
		// $message->setBody('Message content here with HTML', 'text/html');
		$message->addTo($sendTo);
		$message->setFrom(array(Yii::app()->params['noreplyEmail'] => Yii::app()->name ));

		$succ = Yii::app()->mail->send($message);
	}

	function StoreUserSuggest($name, $email, $tel, $subject, $body)
	{
		$model =new Opinion();

		$model->name = $name;
		$model->tel = $tel;
		$model->email = $email;
		$model->title = $subject;
		$model->content = $body;
		$model->Create_at = date('Y-m-d H:i:s');

		$model->save();
	}
}