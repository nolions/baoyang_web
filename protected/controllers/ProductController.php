<?php
class ProductController extends Controller
{
	public $current_controller;

	public function actionIndex($sort=1)
	{
		//get Types of Product based Industry
		$Types = IndustryforType::model()->with('ProductType')->findAll('SubIndustry_id = '.$sort);
		
		//get first's id of Product type
		$TypeFirstId = ($Types!=null)?$Types[0]['Type_id']:0;

		//get sub product type based on type
		$SubType = SubType::model()->findAll('Type_id = '.$TypeFirstId);
		//get first's id of Product sub type
		$SubTypeFirstId = ($SubType!=null)?$SubType[0]['id']:0;

		//get product items list
		$Products = null;
		// $Products = ProductItems::model()->getProductItems($sort, $SubTypeFirstId);
		
		
		$TechnologIndexs = SubTypeforTechnology::model()->with('Technology')->findAll('SubType_id = '.$SubTypeFirstId);

		$Technologys = SubTechnology::model()->findAll();
		$TechnologyIds = array();
		//foreach ($Technologys as $key => $value) {
		//	$TechnologyIds[] = $value['id'];
		//}
		
		$this->render('index', array(
			 'SubIndustry' => $sort,
			 'ProductTypes' => $Types,
			 'ProductSorts' => $SubType,
			 'Products' => $Products,
			 'TechnologIndexs' => $TechnologIndexs,
			 'TechnologyIds' => $TechnologyIds,
			 'bastUrl' => Yii::app()->createUrl("product/ajaxGetProductSort"),
		));
	}

	public function actionAjaxGetProductSort($ProductType)
	{
		$Sorts = SubType::model()->findAll('Type_id = '.$ProductType);

		$SortArray = array();
		foreach ($Sorts as $Sort => $value) 
		{
			$SortArray[] = array('id'=>$value['id'], 'name'=>$value['name']);
		}
		
		//print_r($ProductSortArray);
		echo CJSON::encode($SortArray);
	}

	public function actionAjaxGetProductItemsTable($subIndustryType, $subProductType=0, $subTechnologys=0){
		$subTechnologyArray = explode(",", $subTechnologys);

		$Products = array();
		
		if ($subTechnologys!=null) {
			$tmpProducts = ProductItems::model()->getFilterProductItems($subIndustryType, $subProductType, $subTechnologys);
			
			foreach ($tmpProducts as $key => $value) {
				$ok = false;

				foreach ($subTechnologyArray as $subTechnology => $subTechnologyId) {
					if ($value['SubTechnology_id'] == $subTechnologyId) 
						$ok = true;
					else
						$ok = false;
				}

				if ($ok) {
					array_push($Products, $value);
				} 
			}

		}else{
			$tmpProducts = $tmpProducts = ProductItems::model()->getProductItems($subIndustryType, $subProductType);
			foreach ($tmpProducts as $key => $value) {
				array_push($Products, $value);
			}

			// if ($subProductType =='')
			// 	$tmpProducts = ProductItems::model()->getProductItems($subIndustryType);
			// else{
			// 	$tmpProducts = ProductItems::model()->getProductItems($subIndustryType, $subProductType);
			// }
		}

		//usleep(500000);
        $this->renderPartial('_ProductTable', array(
			'Products' => $Products,
		));
	}

	public function actionAjaxGetTechnolog($ProductType=null, $subProductType=null){
		if($ProductType!=null){
			$SubTypes = SubType::model()->find('Type_id=:Type_id ORDER BY id', array(':Type_id'=>$ProductType));
			$subProductType = $SubTypes->id;
		}
			
		$TechnologIndexs = SubTypeforTechnology::model()->with('Technology')->findAll('SubType_id = '.$subProductType);

		$Technologys = SubTechnology::model()->findAll();
		$TechnologyIds = array();
		foreach ($Technologys as $key => $value) {
			$TechnologyIds[] = $value['id'];
		}

		$this->renderPartial('_TechnologFilter', array(
			'TechnologIndexs' => $TechnologIndexs,
			'TechnologyIds' => $TechnologyIds,
		));
	}
}