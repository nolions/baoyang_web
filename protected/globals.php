<?php
/**
 * Dump pretty var contens
 *
 * @param string $target
 * @return void
 * @author Matata
 */
function dump($target) {
	echo "<pre>";
	CVarDumper::dump($target, 10, true) ;
	echo "</pre>";

}

function gel($a, $b, $gt, $eq, $le){
	if($a == $b)return $eq;
	return ($a > $b)? $gt: $le;
}

/**
 * A short to generate by dibi format
 *
 * @return string
 */
function sql() {
	return SqlBuilder::build(func_get_args());
}

/**
 * Publish asset contents by alias path
 *
 * @param string $alias
 * @return string The path of alias
 */
function publish($alias) {
	// TODO: force publish when in dev enviroment
	return Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias($alias), false, -1, YII_DEBUG);
}

/**
 * Detect the IE version
 * @see http://www.krizka.net/2008/03/15/how-to-detect-internet-exporer-version-with-php/
 * @return integer -1 if not IE
 */
function ieVersion() {
	$match=preg_match('/MSIE ([0-9]\.[0-9])/',$_SERVER['HTTP_USER_AGENT'],$reg);
	if($match==0)
		return -1;
	else
		return floatval($reg[1]);
}
